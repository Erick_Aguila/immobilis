﻿using Dato.ComunaDao;
using Modelo.Comuna;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Comuna
{
    public class ComunaBusiness
    {
        public ComunaDao comunaDao = new ComunaDao();

        public ComunaModel Crear(ComunaModel comunaModel)
        {
            var comuna = comunaDao.Crear(comunaModel);
            return comuna;
        }

        public bool Actualizar(ComunaModel comunaModel)
        {
            var respuesta = comunaDao.Actualizar(comunaModel);
            return respuesta;
        }

        public ComunaModel Leer(int comunaId)
        {
            var comuna = comunaDao.Leer(comunaId);
            return comuna;
        }

        public List<ComunaModel> ObtenerComunaPorRegion(int regionId)
        {
            var lista = comunaDao.ObtenerComunaPorRegion(regionId);
            return lista;
        }

        public List<ComunaModel> Listar()
        {
            var lista = comunaDao.Listar();
            return lista;
        }


    }
}
