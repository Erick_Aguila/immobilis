﻿using Dato.RegionDao;
using Modelo.Region;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Region
{
    public class RegionBusiness
    {
        public RegionDao regionDao = new RegionDao();

        public RegionModel Crear(RegionModel regionModel)
        {
            var region = regionDao.Crear(regionModel);
            return region;
        }

        public bool Actualizar(RegionModel regionModel)
        {
            var region = regionDao.Actualizar(regionModel);
            return region;
        }

        public RegionModel Leer(int regionId)
        {
            var region = regionDao.Leer(regionId);
            return region;
        }

        public List<RegionModel> Lista()
        {
            var lista = regionDao.Listar();
            return lista;
        }



    }
}
