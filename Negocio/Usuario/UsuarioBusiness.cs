﻿using Dato.UsuarioDao;
using Modelo.Usuario;
using Negocio.Enum;
using Negocio.TipoUsuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Usuario
{
    public class UsuarioBusiness
    {
        public UsuarioModel Crear(UsuarioModel usuarioModel)
        {
            usuarioModel.UsuarioId = Guid.NewGuid();
            usuarioModel.Clave = CreateMD5(usuarioModel.Clave);
            UsuarioDao usuarioDao = new UsuarioDao();
            var usuarioCredo = usuarioDao.Crear(usuarioModel);
            return usuarioCredo;
        }

        public bool Actualizar(UsuarioModel usuarioModel)
        {
            UsuarioDao usuarioDao = new UsuarioDao();
            var respuesta = usuarioDao.Actualizar(usuarioModel);
            return respuesta;
        }

        public UsuarioModel Autenticar(UsuarioModel usuarioModel)
        {
            UsuarioDao usuarioDao = new UsuarioDao();
            usuarioModel.Clave = CreateMD5(usuarioModel.Clave);
            usuarioModel = usuarioDao.Autenticar(usuarioModel);
            if (usuarioModel != null)
            {
                TipoUsuarioBusiness tipoUsuarioBusiness = new TipoUsuarioBusiness();
                usuarioModel.TipoUsuario = tipoUsuarioBusiness.Leer(new Modelo.TipoUsuario.TipoUsuarioModel() { TipoUsuarioId = usuarioModel.TipoUsuarioId });
            }
            return usuarioModel;
        }

        public UsuarioModel Leer(UsuarioModel usuarioModel)
        {
            UsuarioDao usuarioDao = new UsuarioDao();
            usuarioModel = usuarioDao.Leer(usuarioModel);
            return usuarioModel;
        }

        public List<UsuarioModel> Listar()
        {
            UsuarioDao usuarioDao = new UsuarioDao();
            var lista = usuarioDao.Listar();
            return lista;
        }

        private static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

    }
}
