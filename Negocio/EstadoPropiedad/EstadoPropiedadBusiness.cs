﻿using Dato.EstadoPropiedadDao;
using Modelo.EstadoPropiedad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.EstadoPropiedad
{
    public class EstadoPropiedadBusiness
    {
        public EstadoPropiedadDao estadoPropiedadDao = new EstadoPropiedadDao();

        public EstadoPropiedadModel Crear(EstadoPropiedadModel estadoPropiedadModel)
        {
            var estadoPropiedad = estadoPropiedadDao.Crear(estadoPropiedadModel);
            return estadoPropiedad;
        }

        public bool Actualizar(EstadoPropiedadModel estadoPropiedadModel)
        {
            var respuesta = estadoPropiedadDao.Actualizar(estadoPropiedadModel);
            return respuesta;
        }

        public EstadoPropiedadModel Leer(int estadoPropiedadId)
        {
            var estadoPropiedad = estadoPropiedadDao.Leer(estadoPropiedadId);
            return estadoPropiedad;
        }

        public bool Eliminar(int estadoPropiedadId)
        {
            var respuesta = estadoPropiedadDao.Eliminar(estadoPropiedadId);
            return respuesta;
        }

        public List<EstadoPropiedadModel> ListarTodo()
        {
            var lista = estadoPropiedadDao.ListarTodo();
            return lista;
        }

    }
}
