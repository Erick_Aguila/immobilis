﻿using Dato.DepartamentoDao;
using Modelo.Departamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Departamento
{
    public class DepartamentoBusiness
    {
        public DepartamentoDao departamentoDao = new DepartamentoDao();

        public DepartamentoModel Crear(DepartamentoModel departamentoModel)
        {
            var departamento = departamentoDao.Crear(departamentoModel);
            return departamentoModel;
        } 

        public bool Actualizar(DepartamentoModel departamentoModel)
        {
            var repsuesta = departamentoDao.Actualizar(departamentoModel);
            return repsuesta;
        }

        public DepartamentoModel Leer(Guid departamentoId)
        {
            var departamento = departamentoDao.Leer(departamentoId);
            return departamento;
        }

    }
}
