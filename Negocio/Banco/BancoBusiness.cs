﻿using Dato.BancoDao;
using Modelo.Banco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Banco
{
    public class BancoBusiness
    {
        public BancoDao bancoDao = new BancoDao();

        public BancoModel Crear(BancoModel bancoModel)
        {
            var banco = bancoDao.Crear(bancoModel);
            return banco;
        }

        public bool Actualizar(BancoModel bancoModel)
        {
            var respuesta = bancoDao.Actualizar(bancoModel);
            return respuesta;
        }

        public BancoModel Leer(int bancoId)
        {
            var banco = bancoDao.Leer(bancoId);
            return banco;
        }

        public List<BancoModel> Listar()
        {
            var lista = bancoDao.Listar();
            return lista;
        }
    }
}
