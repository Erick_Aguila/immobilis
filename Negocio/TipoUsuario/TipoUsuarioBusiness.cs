﻿using Dato.TipoUsuarioDao;
using Modelo.TipoUsuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.TipoUsuario
{
    public class TipoUsuarioBusiness
    {
        public TipoUsuarioModel Leer(TipoUsuarioModel tipoUsuarioModel)
        {
            TipoUsuarioDao tipoUsuarioDao = new TipoUsuarioDao();
            tipoUsuarioModel = tipoUsuarioDao.Leer(tipoUsuarioModel);
            return tipoUsuarioModel;
        }

        public List<TipoUsuarioModel> Listar()
        {
            TipoUsuarioDao tipoUsuarioDao = new TipoUsuarioDao();
            var lista = tipoUsuarioDao.Listar();
            return lista;
        }
    }
}
