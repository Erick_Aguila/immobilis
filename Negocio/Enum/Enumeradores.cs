﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Enum
{
    public enum EnumTipoUsuario
    {
        Administrador = 1,
        Supervisor = 3,
        Ejecutivo = 4,
        Cliente = 5,
        Propietario = 6
    }
}
