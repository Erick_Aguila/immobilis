﻿using Dato.PropiedadDao;
using Modelo.Propiedad;
using Negocio.Banco;
using Negocio.Comuna;
using Negocio.EstadoPropiedad;
using Negocio.TipoPropiedad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Propiedad
{
    public class PropiedadBusiness
    {
        public PropiedadModel Crear(PropiedadModel propiedadModel)
        {
            PropiedadDao propiedadDao = new PropiedadDao();
            var propiedad = propiedadDao.Crear(propiedadModel);
            return propiedad;
        }

        public bool Actualizar(PropiedadModel propiedadModel)
        {
            PropiedadDao propiedadDao = new PropiedadDao();
            var respuesta = propiedadDao.Actualizar(propiedadModel);
            return respuesta;
        }

        public PropiedadModel Leer(Guid propiedadId)
        {
            PropiedadDao propiedadDao = new PropiedadDao();
            var propiedad = propiedadDao.Leer(propiedadId);
            propiedad.Comuna = new ComunaBusiness().Leer(propiedad.ComunaId);
            propiedad.Banco = new BancoBusiness().Leer(propiedad.BancoId);
            return propiedad;
        }

        public PropiedadDisponibleModel ListarPropiedadDisponible()
        {
            PropiedadDao propiedadDao = new PropiedadDao();
            var lista = propiedadDao.ListarPropiedadDisponible();
            return lista;
        }

        public List<PropiedadModel> ListarPropiedadPorEstado(int estadoPropiedadId, Guid usuarioId)
        {
            PropiedadDao propiedadDao = new PropiedadDao();
            var lista = propiedadDao.ListarPropiedadPorEstado(estadoPropiedadId, usuarioId);
            return lista;
        }

        public List<PropiedadModel> ListarDepartamentosReservados()
        {
            PropiedadDao propiedadDao = new PropiedadDao();
            var lista = propiedadDao.ListarDepartamentosReservados();
            foreach (var item in lista)
            {
                item.Comuna = new ComunaBusiness().Leer(item.ComunaId);
                item.Banco = new BancoBusiness().Leer(item.BancoId);
            }
            return lista;
        }

        public bool ReservarPropiedad(PropiedadModel propiedadModel)
        {
            PropiedadDao propiedadDao = new PropiedadDao();
            var reservado = propiedadDao.ReservarPropiedad(propiedadModel);
            return reservado;
        }

        public List<PropiedadModel> ListarTodo()
        {
            PropiedadDao propiedadDao = new PropiedadDao();
            var lista = propiedadDao.ListarTodo();
            foreach (var item in lista)
            {
                item.Comuna = new ComunaBusiness().Leer(item.ComunaId);
                item.Banco = new BancoBusiness().Leer(item.BancoId);
            }
            return lista;
        }

    }
}
