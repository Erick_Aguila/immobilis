﻿using Dato.TipoPropiedadDao;
using Modelo.TipoPropiedad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.TipoPropiedad
{
    public class TipoPropiedadBusiness
    {
        public TipoPropiedadDao tipoPropiedadDao = new TipoPropiedadDao();

        public TipoPropiedadModel Crear(TipoPropiedadModel tipoPropiedadModel)
        {
            var tipoPropiedad = tipoPropiedadDao.Crear(tipoPropiedadModel);
            return tipoPropiedad;
        }

        public bool Actualizar(TipoPropiedadModel tipoPropiedadModel)
        {
            var respuesta = tipoPropiedadDao.Actualizar(tipoPropiedadModel);
            return respuesta;
        }

        public TipoPropiedadModel Leer(int tipoPropiedadId)
        {
            var tipoPropiedad = tipoPropiedadDao.Leer(tipoPropiedadId);
            return tipoPropiedad;
        }

        public List<TipoPropiedadModel> Listar()
        {
            var lista = tipoPropiedadDao.Listar();
            return lista;
        }

    }
}
