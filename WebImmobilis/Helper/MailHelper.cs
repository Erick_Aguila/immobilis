﻿using Cl.CodeBase.CodeCommon.CodeBaseLog;
using Cl.CodeBase.CodeCommon.CodeBaseMail;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Configuration;
using System.Web;
using WebImmobilis.Models.ViewModel.Usuario;

namespace WebImmobilis.Helper
{
    public static class MailHelper
    {
        public static object ApplicationConfig { get; private set; }

        private static void SendMail(string mailFrom, string mailFromName, string mailTo, string subject, string message)
        {
            if (string.IsNullOrEmpty(mailTo))
            {
                return;
            }
            // Mandar correo
            //var smtp = new System.Net.Mail.SmtpClient();
            //Cast the newtwork credentials in to the NetworkCredential class and use it .

            SmtpSection section = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
            string from = section.From;
            string host = section.Network.Host;
            int port = section.Network.Port;
            bool enableSsl = section.Network.EnableSsl;
            string user = section.Network.UserName;
            string password = section.Network.Password;

            MailCredentials credentials = new MailCredentials();
            credentials.loginSmtp = user;
            credentials.passwordSmtp = password;
            Mail mail = new Mail(credentials);
            mail.To = mailTo;
            mail.Host = host;
            mail.Subject = subject;
            BodyMailParserByXSLT parser = new BodyMailParserByXSLT();
            mail.Message = message;
            try
            {
                if (!string.IsNullOrEmpty(mailFrom))
                {
                    mail.From = mailFrom;
                    mail.FromName = mailFromName;
                }
                //Logging.Debug("SEND MAIL. Enviando Correo a " + mailTo + " - ");
                mail.SendMail();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static void SendMailWithAttachment(string mailFrom, string mailFromName, string mailTo, string subject, string message, List<AttachmentElement> attachments)
        {
            if (string.IsNullOrEmpty(mailTo))
            {
                return;
            }
            SmtpSection section = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
            string from = section.From;
            string host = section.Network.Host;
            int port = section.Network.Port;
            bool enableSsl = section.Network.EnableSsl;
            string user = section.Network.UserName;
            string password = section.Network.Password;

            MailCredentials credentials = new MailCredentials();
            credentials.loginSmtp = user;
            credentials.passwordSmtp = password;
            Mail mail = new Mail(credentials);
            mail.To = mailTo;
            mail.Subject = subject;
            if (attachments != null)
            {
                foreach (var attach in attachments)
                {
                    mail.AddAttachFile(attach);
                }
            }
            BodyMailParserByXSLT parser = new BodyMailParserByXSLT();
            mail.Message = message;
            try
            {
                if (!string.IsNullOrEmpty(mailFrom))
                {
                    mail.From = mailFrom;
                    mail.FromName = mailFromName;
                }
                //Logging.Debug("SEND MAIL. Enviando Correo con adjunto a " + mailTo + " - ");
                mail.SendMail();
            }
            catch (Exception e) { throw e; }
        }

        public static bool EnviarMailNuevoUsuario(UsuarioViewModel usuarioViewModel)
        {
            try
            {
                BodyMailParserByXSLT parser = new BodyMailParserByXSLT();
                Hashtable dictionary = new Hashtable();
                //Obtenemos la información detallada               
                dictionary["NombreCompleto"] = usuarioViewModel.NombreCompleto;
                dictionary["Clave"] = usuarioViewModel.Clave;
                var correo = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EsProduccion")) == true ? usuarioViewModel.Email: ConfigurationManager.AppSettings.Get("EmailAdministrador");
                
                string message = parser.ParseBodyMail(GetMailTemplates("UsuarioCreado"), dictionary);
                string mailFrom = ConfigurationManager.AppSettings.Get("AlternativeSendMail");
                string mailFromName = ConfigurationManager.AppSettings.Get("AlternativeSendMailFrom");
                string subject = string.Format(ConfigurationManager.AppSettings.Get("MailSubjectAlertaMora"));
                SendMail(mailFrom, mailFromName, correo, subject, message);
                return true;
            }
            catch (Exception e)
            {
                Logging.Debug("mail " + e.Message + "" + e.StackTrace);
                return false;
            }
        }

        public static bool EnviarMailReservaPropiedad(UsuarioViewModel usuarioViewModel)
        {
            try
            {
                BodyMailParserByXSLT parser = new BodyMailParserByXSLT();
                Hashtable dictionary = new Hashtable();
                //Obtenemos la información detallada               
                dictionary["NombreCompleto"] = usuarioViewModel.NombreCompleto;
                var correo = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EsProduccion")) == true ? usuarioViewModel.Email : ConfigurationManager.AppSettings.Get("EmailAdministrador");

                string message = parser.ParseBodyMail(GetMailTemplates("ReservaPropiedad"), dictionary);
                string mailFrom = ConfigurationManager.AppSettings.Get("AlternativeSendMail");
                string mailFromName = ConfigurationManager.AppSettings.Get("AlternativeSendMailFrom");
                string subject = string.Format(ConfigurationManager.AppSettings.Get("MailSubjectAlertaMora"));
                SendMail(mailFrom, mailFromName, correo, subject, message);
                return true;
            }
            catch (Exception e)
            {
                Logging.Debug("mail " + e.Message + "" + e.StackTrace);
                return false;
            }
        }

        public static bool EnviarMailPropiedadComprada(UsuarioViewModel usuarioViewModel)
        {
            try
            {
                BodyMailParserByXSLT parser = new BodyMailParserByXSLT();
                Hashtable dictionary = new Hashtable();
                //Obtenemos la información detallada               
                dictionary["NombreCompleto"] = usuarioViewModel.NombreCompleto;
                var correo = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EsProduccion")) == true ? usuarioViewModel.Email : ConfigurationManager.AppSettings.Get("EmailAdministrador");

                string message = parser.ParseBodyMail(GetMailTemplates("PropiedadComprada"), dictionary);
                string mailFrom = ConfigurationManager.AppSettings.Get("AlternativeSendMail");
                string mailFromName = ConfigurationManager.AppSettings.Get("AlternativeSendMailFrom");
                string subject = string.Format(ConfigurationManager.AppSettings.Get("MailSubjectAlertaMora"));
                SendMail(mailFrom, mailFromName, correo, subject, message);
                return true;
            }
            catch (Exception e)
            {
                Logging.Debug("mail " + e.Message + "" + e.StackTrace);
                return false;
            }
        }



        public static string GetMailTemplates(string template)
        {
            string entry = ConfigurationManager.AppSettings.Get("MailTemplate" + template);
            if (string.IsNullOrEmpty(entry))
            {
                //throw new Exception(EnumToString.ToString(GeneralEnum.NOT_ENTRY_CONFIG));
            }
            return entry;
        }


    }
}