﻿using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Modelo.Departamento;
using Modelo.Propiedad;
using Negocio.Banco;
using Negocio.Comuna;
using Negocio.Departamento;
using Negocio.Propiedad;
using Negocio.Region;
using Negocio.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebImmobilis.Controllers.Base;
using WebImmobilis.Helper;
using WebImmobilis.Mapper;
using WebImmobilis.Models.ViewModel.Propiedad;
using WebImmobilis.Models.ViewModel.Usuario;

namespace WebImmobilis.Controllers.Propiedad
{
    public class ReservaController : BaseController
    {
        // GET: Reserva
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Departamento()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ListarDepartamentosReservador([DataSourceRequest] DataSourceRequest request)
        {
            PropiedadBusiness propiedadBusiness = new PropiedadBusiness();
            var result = propiedadBusiness.ListarDepartamentosReservados();
            var list = Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            return list;
        }

        private PropiedadViewModel CargarDatosPrincipales()
        {
            PropiedadViewModel propiedadViewModel = new PropiedadViewModel();
            propiedadViewModel.BancoList = new BancoBusiness().Listar();
            propiedadViewModel.RegionList = new RegionBusiness().Lista();
            propiedadViewModel.ComunaList = new ComunaBusiness().Listar();
            return propiedadViewModel;
        }

        [HttpGet]
        public ActionResult GestionarDepartamento(Guid propiedadId)
        {
            IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();

            var datos = CargarDatosPrincipales();
            var propiedadModel = new PropiedadBusiness().Leer(propiedadId);
            var departamentoModel = new DepartamentoBusiness().Leer((Guid)propiedadModel.DepartamentoId);
            DepartamentoViewModel departamentoViewModel = mapper.Map<DepartamentoViewModel>(departamentoModel);
            //Datos propiedad
            departamentoViewModel.RegionId = propiedadModel.RegionId;
            departamentoViewModel.ComunaId = propiedadModel.ComunaId;
            departamentoViewModel.Direccion = propiedadModel.Direccion;
            departamentoViewModel.PrecioUF = (int)propiedadModel.PrecioUf;
            departamentoViewModel.UsuarioId = (Guid)propiedadModel.UsuarioId;

            //Datos combobox
            departamentoViewModel.BancoList = datos.BancoList;
            departamentoViewModel.RegionList = datos.RegionList;
            departamentoViewModel.ComunaList = datos.ComunaList;
            return View(departamentoViewModel);
        }

        [HttpPost]
        public ActionResult GestionarDepartamento(DepartamentoViewModel departamentoViewModel)
        {
            if (ModelState.IsValid)
            {
                PropiedadModel propiedadModel = new PropiedadModel()
                {
                    PropiedadId = departamentoViewModel.PropiedadId,
                    Direccion = departamentoViewModel.Direccion,
                    RegionId = departamentoViewModel.RegionId,
                    ComunaId = departamentoViewModel.ComunaId,
                    DepartamentoId = departamentoViewModel.DepartamentoId,
                    PrecioUf = departamentoViewModel.PrecioUF,
                    EstadoPropiedadId = 4, //Vendido
                    TipoPropiedadId = 1, //DEPARTAMENTO
                    BancoId = 1,
                    BodegaId = null,
                    CasaId = null,
                    EstacionamientoId = null,
                    OficinaId = null,
                    UsuarioId = departamentoViewModel.UsuarioId,
                    GiroNegocioId = null
                };
                PropiedadBusiness propiedadBusiness = new PropiedadBusiness();
                var respuesta = propiedadBusiness.Actualizar(propiedadModel);
                if (respuesta)
                {
                    //Redireccionar
                    ModelState.AddModelError(string.Empty, "Propiedad Vendida");
                    PutMessage("Propiedad Vendida", LevelMessage.Exito);
                    var usuarioModel = new UsuarioBusiness().Leer(new Modelo.Usuario.UsuarioModel() { UsuarioId = departamentoViewModel.UsuarioId });
                    IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();
                    var usuarioViewModel = mapper.Map<UsuarioViewModel>(usuarioModel);
                    MailHelper.EnviarMailPropiedadComprada(usuarioViewModel);
                    return RedirectToAction("Departamento");
                }
                else
                {
                    //Hubo un error al crear la propiedad
                    ModelState.AddModelError(string.Empty, "Hubo un problema al vender la propiedad");
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Por favor, complete los campos y vuelva a intentar.");
            }
            return View(departamentoViewModel);
        }


    }
}