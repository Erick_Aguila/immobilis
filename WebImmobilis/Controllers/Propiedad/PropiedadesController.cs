﻿using AutoMapper;
using Negocio.Banco;
using Negocio.Propiedad;
using Negocio.Usuario;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using WebImmobilis.Controllers.Base;
using WebImmobilis.Helper;
using WebImmobilis.Mapper;
using WebImmobilis.Models.ViewModel.Reservar;
using WebImmobilis.Models.ViewModel.Usuario;

namespace WebImmobilis.Controllers.Propiedad
{
    public class PropiedadesController : BaseController
    {
        // GET: Propiedades
        public ActionResult Index()
        {
            PropiedadBusiness propiedadBusiness = new PropiedadBusiness();
            var lista = propiedadBusiness.ListarPropiedadDisponible();
            ViewBag.ListaDepartamentos = lista.ListaDepartamentos;
            ViewBag.User = GetUser();
            return View();
        }

        public ActionResult Detalle(Guid propiedadId)
        {
            ViewBag.User = GetUser();
            ReservaViewModel reservaViewModel = new ReservaViewModel();
            reservaViewModel.BancoList = new BancoBusiness().Listar();
            reservaViewModel.PropiedadId = propiedadId;
            reservaViewModel.Propiedad = new PropiedadBusiness().Leer(propiedadId);
            return View(reservaViewModel);
        }

        /// <summary>
        /// Método para realizar una reserva de la propiedad
        /// </summary>
        /// <param name="reservaViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ReservarPropiedad(ReservaViewModel reservaViewModel)
        {
            if (ModelState.IsValid)
            {
                var user = GetUser();
                if (user != null)
                {
                    //Comprobar que el cliente tenga saldo disponible
                    var rutCliente = string.Format("{0}{1}", reservaViewModel.Rut, reservaViewModel.RutDv);
                    var client = new HttpClient();
                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings.Get("ApiBancoUrl"));
                    var postTask = client.PostAsJsonAsync<ReservaViewModel>("Banco", new ReservaViewModel() { Clave = reservaViewModel.Clave, MontoPie = reservaViewModel.MontoPie, NombreBanco = reservaViewModel.NombreBanco, Rut = rutCliente });
                    postTask.Wait();
                    var result = postTask.Result;

                    if (result.IsSuccessStatusCode)
                    {
                        var reservado = new PropiedadBusiness().ReservarPropiedad(new Modelo.Propiedad.PropiedadModel() { PropiedadId = reservaViewModel.PropiedadId, UsuarioId = Guid.Parse(user.UserId) });
                        if (reservado)
                        {
                            var usuarioModel = new UsuarioBusiness().Leer(new Modelo.Usuario.UsuarioModel() { UsuarioId = Guid.Parse(user.UserId) });
                            IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();
                            var usuarioViewModel = mapper.Map<UsuarioViewModel>(usuarioModel);
                            MailHelper.EnviarMailReservaPropiedad(usuarioViewModel);
                            PutMessage("Felicidades la propiedad fue reservada, un ejecutivo se contactara contigo", LevelMessage.Exito);
                            return RedirectToAction("Index");
                        }
                    }
                    else
                    {
                        PutMessage("No se pudo hacer la reserva, contactese con su banco", LevelMessage.Error);
                        return RedirectToAction("Detalle", reservaViewModel.PropiedadId);
                    }
                }
            }
            return Json(false);
        }

        public ActionResult Mensaje(string mensaje)
        {
            ViewBag.Mensaje = mensaje;
            return View();
        }

    }
}