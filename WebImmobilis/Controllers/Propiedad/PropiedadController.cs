﻿using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Modelo.Departamento;
using Modelo.Propiedad;
using Negocio.Banco;
using Negocio.Comuna;
using Negocio.Departamento;
using Negocio.Propiedad;
using Negocio.Region;
using Negocio.TipoPropiedad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebImmobilis.Controllers.Base;
using WebImmobilis.Mapper;
using WebImmobilis.Models.ViewModel.Propiedad;

namespace WebImmobilis.Controllers.Propiedad
{
    public class PropiedadController : BaseController
    {
        // GET: Propiedad
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ListarPropiedad([DataSourceRequest] DataSourceRequest request)
        {
            PropiedadBusiness propiedadBusiness = new PropiedadBusiness();
            var result = propiedadBusiness.ListarTodo();
            var list = Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            return list;
        }

        private PropiedadViewModel CargarDatosPrincipales()
        {
            PropiedadViewModel propiedadViewModel = new PropiedadViewModel();
            propiedadViewModel.BancoList = new BancoBusiness().Listar();
            propiedadViewModel.RegionList = new RegionBusiness().Lista();
            propiedadViewModel.ComunaList = new ComunaBusiness().Listar();
            //propiedadViewModel.TipoPropiedadList = new TipoPropiedadBusiness().Listar();
            return propiedadViewModel;
        }

        [HttpGet]
        public ActionResult CrearDepartamento()
        {
            var datos = CargarDatosPrincipales();
            DepartamentoViewModel departamentoViewModel = new DepartamentoViewModel();
            departamentoViewModel.BancoList = datos.BancoList;
            departamentoViewModel.RegionList = datos.RegionList;
            departamentoViewModel.ComunaList = datos.ComunaList;
            return View(departamentoViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CrearDepartamento(DepartamentoViewModel departamentoViewModel)
        {
            if (ModelState.IsValid)
            {
                //Creamos primero el departamento y despues la propiedad
                DepartamentoModel departamentoModel = new DepartamentoModel()
                {
                    DepartamentoId = Guid.NewGuid(),
                    NombreEdificio = departamentoViewModel.NombreEdificio,
                    CantidadDormitorios = departamentoViewModel.cantidadDormitorios,
                    CantidadBanios = departamentoViewModel.cantidadBanios,
                    MetrosCuadrados = departamentoViewModel.MetrosCuadrados,
                    Activo = true,
                    FechaCreacion = DateTime.Now
                };

                DepartamentoBusiness departamentoBusiness = new DepartamentoBusiness();
                var deptoCreado = departamentoBusiness.Crear(departamentoModel);
                if (departamentoModel.Error == 0)
                {
                    //Creamos la propiedad
                    PropiedadModel propiedadModel = new PropiedadModel()
                    {
                        PropiedadId = Guid.NewGuid(),
                        Direccion = departamentoViewModel.Direccion,
                        RegionId = departamentoViewModel.RegionId,
                        ComunaId = departamentoViewModel.ComunaId,
                        DepartamentoId = departamentoModel.DepartamentoId,
                        PrecioUf = departamentoViewModel.PrecioUF,
                        EstadoPropiedadId = 1, //DISPONIBLE
                        TipoPropiedadId = 1, //DEPARTAMENTO
                        BancoId = 1,
                        BodegaId = null,
                        CasaId = null,
                        EstacionamientoId = null,
                        OficinaId = null,
                        UsuarioId = null,
                        GiroNegocioId = null
                    };
                    PropiedadBusiness propiedadBusiness = new PropiedadBusiness();
                    var propiedadCreada = propiedadBusiness.Crear(propiedadModel);
                    if (propiedadCreada.Error == 0)
                    {
                        //Redireccionar
                        ModelState.AddModelError(string.Empty, "Propiedad creado correctamente.");
                        PutMessage("Propiedad Creado correctamente", LevelMessage.Exito);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //Hubo un error al crear la propiedad
                        ModelState.AddModelError(string.Empty, propiedadCreada.Mensaje);
                    }
                }
                else
                {
                    //HUBO UN ERROR AL CREAR EL DEPARTAMENTO
                    ModelState.AddModelError(string.Empty, deptoCreado.Mensaje);
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Por favor, complete los campos y vuelva a intentar.");
            }          
            return View(departamentoViewModel);
        }

        [HttpGet]
        public ActionResult EditarDepartamento(Guid propiedadId)
        {
            IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();

            var datos = CargarDatosPrincipales();
            var propiedadModel = new PropiedadBusiness().Leer(propiedadId);
            var departamentoModel = new DepartamentoBusiness().Leer((Guid)propiedadModel.DepartamentoId);
            DepartamentoViewModel departamentoViewModel = mapper.Map<DepartamentoViewModel>(departamentoModel);
            //Datos propiedad
            departamentoViewModel.RegionId = propiedadModel.RegionId;
            departamentoViewModel.ComunaId = propiedadModel.ComunaId;
            departamentoViewModel.Direccion = propiedadModel.Direccion;
            departamentoViewModel.PrecioUF = (int)propiedadModel.PrecioUf;

            //Datos combobox
            departamentoViewModel.BancoList = datos.BancoList;
            departamentoViewModel.RegionList = datos.RegionList;
            departamentoViewModel.ComunaList = datos.ComunaList;
            return View(departamentoViewModel);
        }

        [HttpPost]
        public ActionResult EditarDepartamento(DepartamentoViewModel departamentoViewModel)
        {
            if (ModelState.IsValid)
            {
                DepartamentoModel departamentoModel = new DepartamentoModel()
                {
                    DepartamentoId = departamentoViewModel.DepartamentoId,
                    NombreEdificio = departamentoViewModel.NombreEdificio,
                    CantidadDormitorios = departamentoViewModel.cantidadDormitorios,
                    CantidadBanios = departamentoViewModel.cantidadBanios,
                    MetrosCuadrados = departamentoViewModel.MetrosCuadrados,
                    Activo = true,
                    FechaCreacion = DateTime.Now
                };

                DepartamentoBusiness departamentoBusiness = new DepartamentoBusiness();
                var deptoCreado = departamentoBusiness.Actualizar(departamentoModel);
                if (deptoCreado)
                {
                    PropiedadModel propiedadModel = new PropiedadModel()
                    {
                        PropiedadId = departamentoViewModel.PropiedadId,
                        Direccion = departamentoViewModel.Direccion,
                        RegionId = departamentoViewModel.RegionId,
                        ComunaId = departamentoViewModel.ComunaId,
                        DepartamentoId = departamentoModel.DepartamentoId,
                        PrecioUf = departamentoViewModel.PrecioUF,
                        EstadoPropiedadId = 1, //DISPONIBLE
                        TipoPropiedadId = 1, //DEPARTAMENTO
                        BancoId = 1,
                        BodegaId = null,
                        CasaId = null,
                        EstacionamientoId = null,
                        OficinaId = null,
                        UsuarioId = null,
                        GiroNegocioId = null
                    };
                    PropiedadBusiness propiedadBusiness = new PropiedadBusiness();
                    var respuesta = propiedadBusiness.Actualizar(propiedadModel);
                    if (respuesta)
                    {
                        //Redireccionar
                        ModelState.AddModelError(string.Empty, "Propiedad actualizada correctamente.");
                        PutMessage("Propiedad Creado correctamente", LevelMessage.Exito);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //Hubo un error al crear la propiedad
                        ModelState.AddModelError(string.Empty, "Hubo un problema al actualizar la propiedad");
                    }
                }
                else
                {
                    //HUBO UN ERROR AL CREAR EL DEPARTAMENTO
                    ModelState.AddModelError(string.Empty, "Hubo un problema al actualizar el departamento");
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Por favor, complete los campos y vuelva a intentar.");
            }
            return View(departamentoViewModel);
        }


    }
}