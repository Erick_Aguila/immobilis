﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WebImmobilis.DAL.Security;

namespace WebImmobilis.Controllers.Base
{
    public class BaseController : Controller
    {
        protected string IdSession = "IdSession";
        //protected string messageResources = ApplicationConfig.GetMessageResource();
        public BaseController()
        {
            SetCulture();
        }
        //public ControlState controlState = new ControlState(System.Web.HttpContext.Current);
        public bool IsInitialize { get; set; }
        private const string ConstMessageKey = "MessageKey";
        private const string ConstMessageCssClass = "MessageCssClass";

        public T GetTempData<T>(string key)
        {
            if (TempData.ContainsKey(key))
            {
                return (T)TempData[key];
            }
            return default(T);
        }

        protected void SetCulture()
        {
            CultureInfo cultureInfo = new CultureInfo("es-CL");
            cultureInfo.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            cultureInfo.DateTimeFormat.DateSeparator = "/";
            cultureInfo.NumberFormat.CurrencyDecimalDigits = 3;
            cultureInfo.NumberFormat.NumberDecimalDigits = 3;
            cultureInfo.NumberFormat.CurrencyDecimalSeparator = ",";
            cultureInfo.NumberFormat.CurrencySymbol = string.Empty;
            cultureInfo.DateTimeFormat.FirstDayOfWeek = DayOfWeek.Monday;
            Thread.CurrentThread.CurrentUICulture = cultureInfo;
            Thread.CurrentThread.CurrentCulture = cultureInfo;
        }

        public void PutTempData<T>(string key, T value)
        {
            TempData.Remove(key);
            TempData.Add(key, value);
        }

        //public void PutMessage(BackendMessage backendMessage, LevelMessage levelMessage)
        //{
        //    var messageText = ConfigurationHelper.Mensaje(backendMessage);
        //    PutTempData(ConstMessageKey, messageText);
        //    PutTempData(ConstMessageCssClass, levelMessage.ToString());
        //}

        public void PutMessage(string messageText, LevelMessage levelMessage)
        {
            PutTempData(ConstMessageKey, messageText);
            PutTempData(ConstMessageCssClass, levelMessage.ToString());
        }

        public void ClearMessage()
        {
            TempData.Remove(ConstMessageKey);
            TempData.Remove(ConstMessageCssClass);
        }

        protected virtual new CustomPrincipal User {
            get {
                return HttpContext.User as CustomPrincipal;
            }
        }

        protected CustomPrincipal GetUser()
        {
            CustomPrincipal user = User;
            return user;
        }

        protected String UploadDir(string fileName)
        {
            return Path.Combine(AppDomain.CurrentDomain.GetData("DataDirectory").ToString(), "Files", fileName);
        }

    }

    public enum LevelMessage
    {
        Exito, Informacion, Error, Advertencia
    }


}