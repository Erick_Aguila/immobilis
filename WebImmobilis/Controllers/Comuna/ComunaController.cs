﻿using Negocio.Comuna;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebImmobilis.Controllers.Comuna
{
    public class ComunaController : Controller
    {
        [HttpGet]
        public JsonResult ObtenerComunaRegion(int regionId)
        {
            ComunaBusiness comunaBusiness = new ComunaBusiness();
            var lista = comunaBusiness.ObtenerComunaPorRegion(regionId);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }
    }
}
