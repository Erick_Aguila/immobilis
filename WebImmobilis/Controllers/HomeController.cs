﻿using Modelo.Usuario;
using Negocio.TipoUsuario;
using Negocio.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebImmobilis.Controllers.Base;
using WebImmobilis.Models.ViewModel;

namespace WebImmobilis.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            ViewBag.User = GetUser();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.User = GetUser();
            return View();
        }

        [HttpPost]
        public ActionResult UsuarioAutenticado()
        {
            var user = GetUser();
            if (user != null)
            {
                return Json(true);
            }
            return Json(false);
        }

        /// <summary>
        /// Vista para listas los conveniode  las apis
        /// </summary>
        /// <returns></returns>
        public ActionResult Convenio()
        {
            return View();
        }


        /// <summary>
        /// Vista para lista los bancos de la api
        /// </summary>
        /// <returns></returns>
        public ActionResult Banco()
        {
            return View();
        }
    }
}
