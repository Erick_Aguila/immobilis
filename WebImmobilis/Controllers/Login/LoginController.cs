﻿using Modelo.Usuario;
using Negocio.Usuario;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebImmobilis.Controllers.Base;
using WebImmobilis.DAL.Security;
using WebImmobilis.Models.Enum;
using WebImmobilis.Models.ViewModel.Login;

namespace WebImmobilis.Controllers.Login
{
    public class LoginController : BaseController
    {
        // GET: Login
        public ActionResult Index()
        {
            LoginViewModel loginViewModel = new LoginViewModel();
            return View(loginViewModel);
        }

        [HttpPost]
        public ActionResult Autenticar(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    UsuarioBusiness usuarioBusiness = new UsuarioBusiness();
                    var usuarioAutenticado = usuarioBusiness.Autenticar(new UsuarioModel() { Email = model.Email, Clave = model.Clave });
                    if (usuarioAutenticado != null)
                    {
                        //Guardar datos de usuario en cookies.
                        return SetAuthenticationAndRedirect("Intranet", "Index", string.Empty, usuarioAutenticado, false, model.Recuerdame);
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Nombre de Usuario o contraseña son inválidas.");
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, "Hubo un problema interno, vuelva a intentar más tarde.");
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Por favor, complete los campos y vuelva a intentar.");
            }
            return View("Index", model);
        }

        private ActionResult SetAuthenticationAndRedirect(string controller, string action, string url, UsuarioModel usuarioModel, bool redireccionParametro, bool recuerdame)
        {
            DoLogin(Response, usuarioModel, Guid.Empty, recuerdame); //, proyectos, modulos);
            if (string.IsNullOrEmpty(url))
            {
                #region código comentado
                //if (redireccionParametro)
                //{
                //    return RedirectToAction(action, controller, new { id = usuarioModel.Email });
                //}
                //else
                //{
                //    return RedirectToAction(action, controller);
                //}
                #endregion

                if (usuarioModel.TipoUsuarioId == (int)EnumTipoUsuario.Administrador || usuarioModel.TipoUsuarioId == (int)EnumTipoUsuario.Supervisor || usuarioModel.TipoUsuarioId == (int)EnumTipoUsuario.Ejecutivo)
                {
                    return RedirectToAction(action, "Intranet");
                }
                else
                {
                    return RedirectToAction(action, "Home");
                }

            }
            else
            {
                return Redirect(url);
            }
        }

        public void DoLogin(HttpResponseBase response, UsuarioModel usuarioModel, Guid rolId, bool recuerdame) //, List<ProyectosModel> proyectos, List<ModulosModel> modulos)
        {
            CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel();
            serializeModel.UserId = usuarioModel.UsuarioId.ToString();
            serializeModel.Name = string.Format("{0} {1}", usuarioModel.Nombres, usuarioModel.ApellidoPaterno);
            serializeModel.Email = usuarioModel.Email;
            serializeModel.RoleId = rolId;
            serializeModel.Recuerdame = recuerdame;
            serializeModel.TipoUsuarioId = usuarioModel.TipoUsuarioId;
            serializeModel.NombreTipoUsuario = usuarioModel.TipoUsuario.Nombre;


            //serializeModel.NombreUsuario = nombreUsuario;
            //serializeModel.Proyectos = proyectos;
            //serializeModel.ModulosMenu = modulos;
            string userData = JsonConvert.SerializeObject(serializeModel);
            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                     1,
                    Guid.NewGuid().ToString(),
                     //serializeModel.UserId.ToString(),
                     DateTime.Now,
                     DateTime.Now.AddMinutes(180),
                     false,
                     userData);
            string encTicket = FormsAuthentication.Encrypt(authTicket);
            HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            response.Cookies.Add(faCookie);
        }

        public ActionResult CerrarSesion()
        {
            //FIX - al hacer esto se cierran todas las sesiones de los usuarios del conectados?
            FormsAuthentication.SignOut();
            return RedirectToAction("Index","Home");
        }

    }
}