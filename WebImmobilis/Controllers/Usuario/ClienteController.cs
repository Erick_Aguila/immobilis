﻿using AutoMapper;
using Modelo.Usuario;
using Negocio.Propiedad;
using Negocio.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebImmobilis.Controllers.Base;
using WebImmobilis.Mapper;
using WebImmobilis.Models.ViewModel.Propiedad;
using WebImmobilis.Models.ViewModel.Usuario;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace WebImmobilis.Controllers.Usuario
{
    public class ClienteController : BaseController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET: Cliente
        public ActionResult Index()
        {
            log.Error("This is my error");
            return View();
        }

        public ActionResult MiPerfil()
        {
            var user = GetUser();
            ViewBag.User = GetUser();
            if (user != null)
            {
                var usuarioModel = new UsuarioBusiness().Leer(new Modelo.Usuario.UsuarioModel() { UsuarioId = Guid.Parse(user.UserId) });
                if (usuarioModel.Error == 0)
                {
                    IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();
                    var usuarioViewModel = mapper.Map<UsuarioViewModel>(usuarioModel);

                    PropiedadBusiness propiedadBusiness = new PropiedadBusiness();

                    //Obtenemos lista de departamentos en reserva
                    var listaDepartamentoEnReserva = propiedadBusiness.ListarPropiedadPorEstado(3, Guid.Parse(user.UserId));
                    usuarioViewModel.ListaDepartamentosEnReserva = new List<DepartamentoViewModel>();
                    //usuarioViewModel.ListaDepartamentosEnReserva = mapper.Map<List<DepartamentoViewModel>>(listaDepartamentoEnReserva);
                    foreach (var item in listaDepartamentoEnReserva)
                    {
                        DepartamentoViewModel departamentoViewModel = new DepartamentoViewModel()
                        {
                            Activo = item.Departamento.Activo,
                            BancoId = item.BancoId,
                            cantidadBanios = item.Departamento.CantidadBanios,
                            cantidadDormitorios = item.Departamento.CantidadDormitorios,
                            ConservadorBienes = item.ConservadorBienes,
                            DepartamentoId = (Guid)item.DepartamentoId,
                            EstadoPropiedadId = item.EstadoPropiedadId,
                            MetrosCuadrados = item.Departamento.MetrosCuadrados,
                            NombreEdificio = item.Departamento.NombreEdificio,
                            PrecioUF = (int)item.PrecioUf,
                            PropiedadId = item.PropiedadId
                        };
                        usuarioViewModel.ListaDepartamentosEnReserva.Add(departamentoViewModel);
                    }

                    //Obtenemos lista de departamentos comprados
                    var listaDepartamentoComprados = propiedadBusiness.ListarPropiedadPorEstado(4, Guid.Parse(user.UserId));
                    usuarioViewModel.ListaDepartamentosComprados = new List<DepartamentoViewModel>();
                    //usuarioViewModel.ListaDepartamentosComprados = mapper.Map<List<DepartamentoViewModel>>(listaDepartamentoComprados);
                    foreach (var item in listaDepartamentoComprados)
                    {
                        DepartamentoViewModel departamentoViewModel = new DepartamentoViewModel()
                        {
                            Activo = item.Departamento.Activo,
                            BancoId = item.BancoId,
                            cantidadBanios = item.Departamento.CantidadBanios,
                            cantidadDormitorios = item.Departamento.CantidadDormitorios,
                            ConservadorBienes = item.ConservadorBienes,
                            DepartamentoId = (Guid)item.DepartamentoId,
                            EstadoPropiedadId = item.EstadoPropiedadId,
                            MetrosCuadrados = item.Departamento.MetrosCuadrados,
                            NombreEdificio = item.Departamento.NombreEdificio,
                            PrecioUF = (int)item.PrecioUf,
                            PropiedadId = item.PropiedadId
                        };
                        usuarioViewModel.ListaDepartamentosComprados.Add(departamentoViewModel);
                    }

                    return View(usuarioViewModel);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, usuarioModel.Mensaje);
                    return View(new UsuarioViewModel());
                }
            }
            return View("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Actualizar(UsuarioViewModel usuarioViewModel)
        {
            if (ModelState.IsValid)
            {
                usuarioViewModel.Rut = $"{usuarioViewModel.Rut}-{usuarioViewModel.RutDv}";
                IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();
                var usuarioModel = mapper.Map<UsuarioModel>(usuarioViewModel);                

                UsuarioBusiness usuarioBusiness = new UsuarioBusiness();
                var usuarioActualizado = usuarioBusiness.Actualizar(usuarioModel);
                if (usuarioActualizado)
                {
                    ModelState.AddModelError(string.Empty, "Datos actualizados correctamente.");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Hubo un problema interno, vuelva a intentar más tarde.");
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Corriga los campos y vuelva intentar.");
            }
            return View("MiPerfil", usuarioViewModel);
        }

    }
}