﻿using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Modelo.Usuario;
using Negocio.TipoUsuario;
using Negocio.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebImmobilis.Controllers.Base;
using WebImmobilis.DAL.Security;
using WebImmobilis.Helper;
using WebImmobilis.Mapper;
using WebImmobilis.Models.Enum;
using WebImmobilis.Models.ViewModel.Usuario;

namespace WebImmobilis.Controllers.Usuario
{
    public class UsuarioController : BaseController
    {
        // GET: Usuario
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ListarUsuario([DataSourceRequest] DataSourceRequest request)
        {
            UsuarioBusiness usuarioBusiness = new UsuarioBusiness();
            var result = usuarioBusiness.Listar();
            var list = Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            return list;
        }

        [HttpGet]
        public ActionResult CrearCliente()
        {
            return View(new UsuarioViewModel() { TipoUsuarioId = (int)EnumTipoUsuario.Cliente, NombreVista = "CrearCliente"});
        }

        [HttpGet]
        public ActionResult Crear()
        {
            UsuarioViewModel usuarioViewModel = new UsuarioViewModel();
            usuarioViewModel.TipoUsuarioList = new TipoUsuarioBusiness().Listar();
            usuarioViewModel.NombreVista = "Crear";
            usuarioViewModel.Clave = "Default";
            return View(usuarioViewModel);
        }

        /// <summary>
        /// Método para crear Usuario para el sistema
        /// </summary>
        /// <param name="usuarioViewModel">Objeto que contiene los datos a ingresar</param>
        /// <returns>Retorna una respuesta si el usuario pudo ser ingresadoc correctamente</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Crear(UsuarioViewModel usuarioViewModel)
        {
            if (ModelState.IsValid)
            {
                IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();
                var usuarioModel = mapper.Map<UsuarioModel>(usuarioViewModel);
                usuarioModel.Rut = string.Format("{0}-{1}", usuarioViewModel.Rut, usuarioViewModel.RutDv);

                string claveUsuario = Guid.NewGuid().ToString().Substring(0, 8);
                if (usuarioModel.TipoUsuarioId == (int)EnumTipoUsuario.Administrador || usuarioModel.TipoUsuarioId == (int)EnumTipoUsuario.Ejecutivo || usuarioModel.TipoUsuarioId == (int)EnumTipoUsuario.Supervisor)
                {
                    usuarioModel.Clave = claveUsuario;
                }

                var usuarioCreado = new UsuarioBusiness().Crear(usuarioModel);
                if (usuarioCreado.Error == 0)
                {
                    ModelState.AddModelError(string.Empty, "Usuario creado correctamente.");
                    //Mensaje de usuario creado con exito
                    if (usuarioCreado.TipoUsuarioId == (int)EnumTipoUsuario.Cliente || usuarioCreado.TipoUsuarioId == (int)EnumTipoUsuario.Propietario)
                    {
                        PutMessage("Usuario Creado correctamente", LevelMessage.Exito);
                        return View("CrearCliente");
                    }
                    else
                    {
                        usuarioViewModel.Clave = claveUsuario;
                        bool correoEnviado = MailHelper.EnviarMailNuevoUsuario(usuarioViewModel);
                        if (correoEnviado)
                        {
                            PutMessage("Usuario Creado correctamente", LevelMessage.Exito);
                        }
                        else
                        {
                            PutMessage("Usuario Creado correctamente, pero no se ha enviado el correo", LevelMessage.Advertencia);
                        }
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, usuarioCreado.Mensaje);
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Por favor, complete los campos y vuelva a intentar.");
            }
            usuarioViewModel.TipoUsuarioList = new TipoUsuarioBusiness().Listar();
            return View(usuarioViewModel.NombreVista,usuarioViewModel);
        }

        public ActionResult Editar(Guid usuarioId)
        {
            var usuarioModel = new UsuarioBusiness().Leer(new UsuarioModel() { UsuarioId = usuarioId });
            IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();
            var usuarioViewModel= mapper.Map<UsuarioViewModel>(usuarioModel);

            var rutSeparado = usuarioViewModel.Rut.Split('-');
            usuarioViewModel.Rut = rutSeparado[0];
            usuarioViewModel.RutDv = Convert.ToInt32(rutSeparado[1]);
            usuarioViewModel.TipoUsuarioList = new TipoUsuarioBusiness().Listar();
            return View(usuarioViewModel);
        }

        /// <summary>
        /// Método para poder editar los datos de un Usuario
        /// </summary>
        /// <param name="usuarioViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(UsuarioViewModel usuarioViewModel)
        {
            if (ModelState.IsValid)
            {
                IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();
                var usuarioModel = mapper.Map<UsuarioModel>(usuarioViewModel);
                usuarioModel.Rut = string.Format("{0}-{1}", usuarioViewModel.Rut, usuarioViewModel.RutDv);

                var usuarioActualizado = new UsuarioBusiness().Actualizar(usuarioModel);
                if (usuarioActualizado)
                {
                    PutMessage("Usuario actualizado correctamente", LevelMessage.Exito);
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError(string.Empty, "Los datos del usuario no se actualizaron, vuelva a intentar más tarde.");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Por favor, complete los campos y vuelva a intentar.");
            }
            usuarioViewModel.TipoUsuarioList = new TipoUsuarioBusiness().Listar();
            return View("Editar", usuarioViewModel);
        }

        public CustomPrincipal ObtenerUsuarioLogueado()
        {
            var usuario = GetUser();
            return usuario;
        }

    }
}