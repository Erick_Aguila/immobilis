﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebImmobilis.Controllers.Base;
using WebImmobilis.DAL.Security;

namespace WebImmobilis.Controllers.Intranet
{
    [CustomAuthorize]
    public class IntranetController : BaseController
    {
        // GET: Intranet
        public ActionResult Index()
        {
            return View();
        }
    }
}