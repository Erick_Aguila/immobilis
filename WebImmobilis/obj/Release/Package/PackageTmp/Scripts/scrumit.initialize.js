//Generales -- respuestas Http
var HttpResponse = {
    NotAcceptable: "Not Acceptable",
    PreconditionFailed: "Precondition Failed",
    NonAuthoritativeInformation : "NonAuthoritativeInformation",
    Conflict: "Conflict", ExpectationFailed:"Expectation Failed"
}
var viewModelType = {
    Asegurado: "Asegurado",
    Pagador: "Pagador",
    Proponente:"Proponente"
}

var isMobile = false;
var ventanaSucursal = 0;
var anchoGrilla = 0;
var habilitaAjaxLoading = true;

$(document).ready(function () {
    window.history.pushState(null, "", window.location.href);
    window.onpopstate = function () {
        window.history.pushState(null, "", window.location.href);
    };

    function disableBack() { window.history.forward() }

    window.onload = disableBack();
    window.onpageshow = function (evt) { if (evt.persisted) disableBack() }

    $('body').on('click', function (e) {
        $('[data-toggle="popover"]').each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
});


$(function () {
    //kendo.culture("es-CL");
    if (navigator.userAgent.match(/Mobi/)) {
        isMobile = true;
    }
    setTimeout("initializaPlaceholder()", 500);
    $("#close_message").attr("title", "cerrar mensaje");
    $("#summaryValidator,#summaryValidatorPopUp, .summaryValidatorOpcional").not(".no-dismisable").prepend('<button type="button" class="close close-message" id="btnCloseMessage" aria-hidden="true">&times;</button>');
    //$(".validation-summary-errors").prepend('<button type="button" class="close" id="btnCloseMessage" aria-hidden="true">&times;</button>');

    //$("#close_message").parent().delay(5000).slideUp("slow");
    $("#close_message").click(function () {
        $(this).parent().slideUp("slow");
    });

    $("#btnCloseMessage, .close-message").click(function () {
        $(this).parent().removeClass("validation-summary-errors").addClass("validation-summary-valid");
    });

    var isInIFrame = (window.location != window.parent.location);
    if (isInIFrame == true) {
        $(".highlight").removeClass("hide").addClass("visible-xs").hide().slideDown();
    }

    $(".form-rut").keyup(function () {
        var dvRut = jQuery.Rut.getDigito($(this).val());
        $(this).parent().find(".form-dvrut").val(dvRut);
        LimpiarDigitoVerificador($(this));
    });

    $(".form-rut").blur(function () {
        var dvRut = jQuery.Rut.getDigito($(this).val());
        $(this).parent().find(".form-dvrut").val(dvRut);
        LimpiarDigitoVerificador($(this));
    });

    $body = jQuery("body");
    jQuery(document).on({
        ajaxStart: function () { if (habilitaAjaxLoading) { $body.addClass("loading"); } },
        ajaxStop: function () { if ($body.hasClass("loading")) { $body.removeClass("loading") }; }
    });

    $(window).on('beforeunload', function () {
         if (habilitaAjaxLoading) { $body.addClass("loading"); } 
    });
    //preloadImage('ajax-loader.gif');
    //Aplicamos las reglas de digitaci�n segun tipo de input o su class name
    validaCamposIngreso();




    // load a locale
    numeral.register('locale', 'cl', {
        delimiters: {
            thousands: '.',
            decimal: ','
        },
        abbreviations: {
            thousand: 'k',
            million: 'm',
            billion: 'b',
            trillion: 't'
        },
        ordinal: function (number) {
            return number === 1 ? 'er' : '�me';
        },
        currency: {
            symbol: '�'
        }
    });

    // switch between locales
    numeral.locale('cl');

});


//Elimina el 0 cuando no hay rut
function LimpiarDigitoVerificador(elemento) {
    if (elemento.val() == "") {
        elemento.parent().find(".form-dvrut").val("");
    }
}

function initializaPlaceholder() {
    $('input:not(".fecha")').placeholder();
    $("textarea").placeholder();
    window.parent.scrollTo(0, 50);
    //$('body').scrollTo(140, { duration: 1000 });
}

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}
function convertToUtc(str) {
    if (str != null) {
        var date = new Date(str);
        var year = date.getUTCFullYear();
        var month = date.getUTCMonth() + 1;
        var dd = date.getUTCDate();
        var hh = date.getUTCHours();
        var mi = date.getUTCMinutes();
        var sec = date.getUTCSeconds();
        theDate = year + "-" + pad(month, 2) + "-" + pad(dd, 2);
        theTime = " 00:00:00";
        return [theDate, theTime].join("T");
    } else {
        return str;
    }
}

function validaCamposIngreso() {
    $(".char").alpha();
    $(".form-onlychar").alpha();
    $(".email").alpha({ allow: "@@.0123456789_-", ichars: '-�\~`=?*�+}{)(/&%$#"!��[];,:��<>|�~`\^\'.\\' });
    $(".form-patente").alpha({ allow: "0123456789", ichars: '-�\~`=?*�+}{)(/&%$#"!��[];,:��<>|�~`\^\'.\\' });
    //$(".form-rut").numeric({ decimal: false, negative: false });
    //$(".form-number").numeric({ decimal: false, negative: false });
    $(".form-rut").number({ allow: "1", disallow: "\.-", allowMinus: false});
    $(".form-number").number({ allow: "1", disallow: "\.-", allowMinus: false });
    $(".fecha").number({ allow: "/", disallow: "." });
    $(".form-decimal").number({ allow: ",", disallow: "\.-", allowMinus:false});
    $(".especial").alphanum({ allowNumeric: true, allow: ".,?%-+$:/" });
    $(".no-caracteres-especiales").alphanum({ allowNumeric: true, disallow: "\W" });
    $(".no-caracteres-especiales-direccion").alphanum({ allowNumeric: true, allow: ".,", disallow: "\W" });

    //Se incorporan por data

    $("input").filter("[data-datatype='alpha']").alpha();
    $("input").filter("[data-datatype='email']").alpha({ allow: "@@.0123456789_-", ichars: '-�\~`=?*�+}{)(/&%$#"!��[];,:��<>|�~`\^\'.\\' });
    $("input").filter("[data-datatype='patente']").alpha({ allow: "0123456789", ichars: '-�\~`=?*�+}{)(/&%$#"!��[];,:��<>|�~`\^\'.\\' });
    $("input").filter("[data-datatype='number']").number({ allow: "1", disallow: "\.-", allowMinus: false });
    $("input").filter("[data-datatype='fecha']").number({ allow: "/", disallow: "." });
    $("input").filter("[data-datatype='decimal']").number({ allow: ",", disallow: "\.-", allowMinus: false });
    $("input").filter("[data-datatype='especial']").alphanum({ allowNumeric: true, allow: ".,?%-+$:/" });
    $("input").filter("[data-datatype='no-caracteres-especiales']").alphanum({ allowNumeric: true, disallow: "\W" });
    $("input").filter("[data-datatype='direccion']").alphanum({ allowNumeric: true, allow: ".,", disallow: "\W" });
}

jQuery.extend(jQuery.validator.methods, {
    number: function (value, element) {
        return this.optional(element)
         || /^-?(?:\d+|\d{1,3}(?:\.\d{3})+)(?:[,.]\d+)?$/.test(value);
    }
});

// determanes if text has ellipsis
function isEllipsisActive(e) {
    return e.offsetWidth < e.scrollWidth;
}

function initializeTooltip(element, filter) {
    return element.kendoTooltip({
        autoHide: true,
        filter: filter,
        callout: false,
        show: function (e) {
            e.sender.popup.wrapper.css("margin-top", -15);
        },
        content: function (e) {
            var target = e.target,
                tooltip = e.sender,
                tooltipText = "";

            if (isEllipsisActive(target[0])) {
                tooltipText = $(target).text();
            }
            tooltip.popup.unbind("open");

            tooltip.popup.bind("open", function (arg) {
                tooltip.refreshTooltip = false;

                if (!isEllipsisActive(target[0])) {
                    arg.preventDefault();
                } else if (tooltipText !== $(target).text()) {
                    tooltip.refreshTooltip = true;
                }
            });

            return tooltipText;
        },
        show: function () {
            if (this.refreshTooltip) {
                this.refresh();
            }
        }
    }).data("kendoTooltip");
};


function initializeMessageTooltip(element, filter) {
    return element.kendoTooltip({
        autoHide: true,
        filter: filter,
        callout: false,
        content: function (e) {
            var target = e.target,
                tooltip = e.sender,
                tooltipText = "";

            tooltipText = $(target).attr("data-title");

            tooltip.popup.unbind("open");

            tooltip.popup.bind("open", function (arg) {
                tooltip.refreshTooltip = false;

                if (tooltipText !== $(target).attr("data-title")) {
                    tooltip.refreshTooltip = true;
                }
            });

            return tooltipText;
        },
        show: function () {
            if (this.refreshTooltip) {
                this.refresh();
            }
        }
    }).data("kendoTooltip");
};

if (typeof (jQuery) !== "undefined" && typeof (jQuery.validator) !== "undefined") {
    (function ($) {
        $.validator.addMethod('date', function (value, element) {
            var isChrome = window.chrome;
            // make correction for chrome

            if (isChrome) {
                return this.optional(element) || isDate(value);
            } else if (isMobile) {
                return this.optional(element) || isDate(value);
            }
                // leave default behavior
            else {
                return this.optional(element) || isDate(value);
                //return this.optional(element) || !/Invalid|NaN/.test(new Date(value));
            }
        });
    })(jQuery);


    jQuery.validator.prototype.subset = function (container) {
        var ok = true;
        var self = this;
        $(container).find(':input').each(function () {
            if (!self.element($(this))) ok = false;
        });
        return ok;
    }

}
formatSuraPesos = function (n) {

    return numeral(n).format('0,0');
};

formatSura = function (n) {

    return numeral(n).format('0,0.000');
};

formatSuraDosDecimales = function (n) {

    return numeral(n).format('0,0.00');
};

function formatPorcentaje(numero) {
    var amt = parseFloat(numero);
    return amt.toFixed(2).toString().replace(".", ",");
}

function formatDecimal(numero) {
    var amt = parseFloat(numero);
    return amt.toFixed(3).toString().replace(".",",");
}
Number.prototype.format = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&.');
};

(function ($) {
    $.fn.extend({

        bs_construir_mensaje: function (message)
        {
            if ($.isArray(message)) {
                mensaje = '';
                for (i = 0; i < message.length; i++) {
                    mensaje += '<li>' + message[i] + '</li>';
                }
            } else {
                mensaje = message;
            }
            return mensaje;
        },
        bs_close: function () {
            $(this).html(''); 
        },
        bs_alert: function (message, title) {
            var cls = 'alert-danger';
            var html = '<div class="alert ' + cls + ' alert-dismissable validation-summary-errors"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
            if (typeof title !== 'undefined' && title !== '') {
                html += '<h4>' + title + '</h4>';
            }
            mensaje = this.bs_construir_mensaje(message);
            html += '<span>' + mensaje + '</span></div>';
            $(this).html(html);
            $(this).slideUp(0, function () { $(this).slideDown(600); });
        },
        bs_warning: function (message, title) {
            var cls = 'alert-warning';
            var html = '<div class="alert ' + cls + ' alert-dismissable validation-summary-errors"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
            if (typeof title !== 'undefined' && title !== '') {
                html += '<h4>' + title + '</h4>';
            }
            mensaje = this.bs_construir_mensaje(message);
            html += '<span>' + mensaje + '</span></div>';
            $(this).html(html);
            $(this).slideUp(0, function () { $(this).slideDown(600); });
        },
        bs_info: function (message, title) {
            var cls = 'alert-info';
            var html = '<div id="client-message" class="alert ' + cls + ' alert-dismissable validation-summary-errors"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
            if (typeof title !== 'undefined' && title !== '') {
                html += '<h4>' + title + '</h4>';
            }
            html += '<span>' + message + '</span></div>';
            $(this).html(html);
            $(this).slideUp(0, function () { $(this).slideDown(600); });
        },
        bs_success: function (message, title) {
            var cls = 'alert-success';
            var html = '<div id="client-message" class="alert ' + cls + ' alert-dismissable validation-summary-errors"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
            if (typeof title !== 'undefined' && title !== '') {
                html += '<h4>' + title + '</h4>';
            }
            mensaje = this.bs_construir_mensaje(message);
            html += '<span>' + mensaje + '</span></div>';
            $(this).html(html);
            $(this).slideUp(0, function () { $(this).slideDown(600); });
        }

    });
})(jQuery);



function habilitaBotonera() {
    $("#bt_ExportarExcel").removeAttr("disabled");
    $("#bt_ExportarExcel").removeClass("disabled");
    $("#bt_Imprimir").removeAttr("disabled");
    $("#bt_Imprimir").removeClass("disabled");
}

function deshabilitaBotonera() {
    $("#bt_ExportarExcel").attr("disabled", "disabled");
    $("#bt_ExportarExcel").removeClass("disabled").addClass("disabled");
    $("#bt_Imprimir").attr("disabled", "disabled");
    $("#bt_Imprimir").removeClass("disabled").addClass("disabled");
}

function OcultarValidationSummary() {
    $('.validation-summary-errors').addClass('validation-summary-valid');
    $('.validation-summary-errors').removeClass('validation-summary-errors');

}

function camposEmision() {
    $body = jQuery("body");
    $body.removeClass("loading").addClass("loading");
    $(".col-check").hide();
    $(".btn.btn-default").hide();
    $("#link").hide();
    $("input[type='radio']", $("form")).each(function () {
        if ($(this).is(":checked")) {
            $(this).next().addClass("control-label").show();
        }
        else {
            $(this).next().hide();
        }
        $(this).hide();
    });
    $("div.radio").removeClass("radio");
    $("span").removeClass("k-picker-wrap k-state-default").addClass("no-hover light-label");
    $("input[type='text'], input[type='email'], input[type='number']", $("form")).each(function () {
        $("<label />", { text: this.value, "class": "control-label light-label" }).insertAfter(this);
        $(this).hide();
    });

    $("span.k-widget.k-dropdown.k-header", $("form")).each(function () {
        $(this).hide();
        $(this).next().html($(this).find("span span.k-input").html());
    });

    //$(".k-dropdown-wrap.k-state-default").css("border","none");

    $("select:not([size])").each(function () {
        if ($(this).parent().hasClass("col-md-2") || $(this).parent().hasClass("col-md-3")) {
            $("<label />", { text: $("#" + this.id + ' :selected').text(), "class": "control-label control-info" }).insertAfter($(this));
            $(this).hide();      
        } else {

            $("<label />", { text: $("#" + this.id + ' :selected').text(), "class": "control-label control-info" }).insertAfter($(this).parent());
            $(this).parent().hide();
        }
    });
    $("#contenidos").slideDown();
    $(".AddArchivo").show();
    $(".k-select").hide();
    $(".k-picker-wrap").css("border-color", "transparent");
    $(".checkEspecialDatosPersonales").hide();
    $(".btn-consulta-rut").hide();
    $(".k-dropdown").hide();
    setTimeout(function () {
        camposEmisionKendo(1)
    }, 0);
}

function cerrarCarga() {
    $body = jQuery("body");
    $body.removeClass("loading");
}
function camposEmisionKendo(iteracion) {
    $(".k-dropdown").show();
    $("span.k-widget.k-dropdown.k-header", $("form")).each(function () {
        $(this).hide();
        $(this).next().html($(this).find("span span.k-input").html());
    });
    
    iteracion++;
    if (iteracion < 5000) {
        setTimeout(function () {
            camposEmisionKendo(iteracion)
        }, 1);
    }
    setTimeout(function () {
        cerrarCarga(1)
    }, 2500)
}

