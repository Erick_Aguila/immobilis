﻿if (typeof (jQuery) !== "undefined" && typeof (jQuery.validator) !== "undefined") {

    (function ($) {
        $.validator.addMethod('requiredif', function (value, element, parameters) {
            var id = parameters['dependentproperty'];
            // get the target value (as a string, as that's what actual value will be)
            var targetvalue = parameters['targetvalue'];
            targetvalue = (targetvalue == null ? '' : targetvalue).toString();
            actualvalue = jQuery("input:radio[name="+id+"]:checked").val();
            // get the actual value of the target control
            // if the condition is true, reuse the existing required field validator functionality
            if (targetvalue === actualvalue)
                return $.validator.methods.required.call(this, value, element, parameters);

            return true;
        });
        $.validator.unobtrusive.adapters.add('requiredif', ['dependentproperty', 'targetvalue'], function (options) {

            options.rules['requiredif'] = options.params;
            if (options.message) {
                options.messages['requiredif'] = options.message;
            }

        });


        $.validator.addMethod('requirediff', function (value, element, parameters) {
            var id = parameters['dependentproperty'];
            // get the target value (as a string, as that's what actual value will be)
            var targetvalue = parameters['targetvalue'];
            targetvalue = (targetvalue == null ? '' : targetvalue).toString();
            actualvalue = jQuery("input:radio[id=" + id + "]:checked").val().toLowerCase();
            // get the actual value of the target control
            // if the condition is true, reuse the existing required field validator functionality
            if (targetvalue === actualvalue) {
                if (value == "-1") {
                    return false;
                }
                return $.validator.methods.required.call(this, value, element, parameters);
            }
            return true;
        });
        $.validator.unobtrusive.adapters.add('requirediff', ['dependentproperty', 'targetvalue'], function (options) {

            options.rules['requirediff'] = options.params;
            if (options.message) {
                options.messages['requirediff'] = options.message;
            }

        });



        $.validator.addMethod('requiredifNatural', function (value, element, parameters) {
            var id = parameters['dependentproperty'];
            // get the target value (as a string, as that's what actual value will be)
            var targetvalue = parameters['targetvalue'];
            targetvalue = (targetvalue == null ? '' : targetvalue).toString();
            actualvalue = jQuery("#" + id + "").val();
            
            // get the actual value of the target control
            // if the condition is true, reuse the existing required field validator functionality
            var ruts = actualvalue.split("-");
            
            rut = $.fn.Rut.quitarFormato(ruts[0]);
            
            if (targetvalue >= rut)
                return $.validator.methods.required.call(this, value, element, parameters);

            return true;
        });
        $.validator.unobtrusive.adapters.add('requiredifnatural', ['dependentproperty', 'targetvalue'], function (options) {

            options.rules['requiredifNatural'] = options.params;
            if (options.message) {
                options.messages['requiredifNatural'] = options.message;
            }

        });

        //Validacion de la tarjeta de credito
        $.validator.addMethod('specialcreditcard', function (value, element, parameters) {
            var id = parameters['dependentproperty'];
            if (!validateCard(id, element.id)) {
                return false;
            }
            return true;
        });
        $.validator.unobtrusive.adapters.add('specialcreditcard', ['dependentproperty'], function (options) {

            options.rules['specialcreditcard'] = options.params;
            if (options.message) {
                options.messages['specialcreditcard'] = options.message;
            }

        });
        //fin validacion tdc


        $.validator.addMethod('isrut', function (value, element, parameters) {
            if (value != null && value.length > 1) {
                return $.fn.Rut.validar(value);
            }
            return true;
        });
        $.validator.unobtrusive.adapters.add('isrut', function (options) {
            options.rules['isrut'] = options.params;
            if (options.message) {
                options.messages['isrut'] = options.message;
            }

        });

        jQuery.validator.addMethod("multiemails",
             function (value, element) {
                 if (this.optional(element)) // return true on optional element
                     return true;
                 var emails = value.split(/[;,]+/); // split element by , and ;
                 valid = true;
                 for (var i in emails) {
                     value = emails[i];
                     valid = valid &&
                             jQuery.validator.methods.email.call(this, $.trim(value), element);
                 }
                 return valid;
             },

           jQuery.validator.messages.multiemails
        );

        $.validator.unobtrusive.adapters.add('multiemails', function (options) {
            options.rules['multiemails'] = options.params;
            if (options.message) {
                options.messages['multiemails'] = options.message;
            }

        });

        $.validator.addMethod('ispatente', function (value, element, parameters) {
            if (value != null && value.length > 1) {
                //Regex regex = new Regex(@"[^. \s'@AEIMNOQUaeimnoqu,0123456789-]{4}\d{2})|([^.\s'@,0123456789-]{2}\d{4})|([^.\s'@,0123456789-]{3}\d{3})");

                return value.match(/^[a-z]{2}[0-9]{2}[0-9]{2}|[b-d,f-h,j-l,p,r-t,v-z]{2}[b-d,f-h,j-l,p,r-t,v-z]{2}[0-9]{2}|[b-d,f-h,j-l,p,r-t,v-z]{3}[0-9]{3}$/i);
            }           
            return true;
        });

        $.validator.unobtrusive.adapters.add('ispatente', function (options) {
            options.rules['ispatente'] = options.params;
            if (options.message) {
                options.messages['ispatente'] = options.message;
            }
        });

        $.validator.addMethod('isformatomotor', function (value, element, parameters) {
            if (value != null && value.length > -1) {                
                if (!value.match(/\W/)) {
                    return !value.match(/([a-z0-9])\1{4}/);
                }
            }
        });

        $.validator.unobtrusive.adapters.add('isformatomotor', function (options) {
            options.rules['isformatomotor'] = options.params;
            if (options.message) {
                options.messages['isformatomotor'] = options.message;
            }

        });

        $.validator.addMethod('isdate', function (value, element, parameters) {
            if (value != null && value.lenght > 1) {
                return isDate(value);
            }
            return true;
        });
        $.validator.unobtrusive.adapters.add('isdate', function (options) {

            if (options.message) {
                options.messages['isdate'] = options.message;
            }

        });
      
        jQuery.validator.addMethod("emailmsj",
            function (value, element) {
                if (this.optional(element)) // return true on optional element
                    return true;
                var emails = value.split(/[;,]+/); // split element by , and ;
                valid = true;
                for (var i in emails) {
                    value = emails[i];
                    valid = valid &&
                            jQuery.validator.methods.email.call(this, $.trim(value), element);
                }
                return valid;
            },

          jQuery.validator.messages.multiemails
       );

        $.validator.unobtrusive.adapters.add('emailmsj', function (options) {
            options.rules['emailmsj'] = options.params;
            if (options.message) {
                options.messages['emailmsj'] = options.message;
            }

        });

    })(jQuery);

}

function isDate(value) {


    var pattern1 = new RegExp("^(0[0-9]|[1-2][0-9]|30|31)/(0[13-9]|1[0-2])/[1-9][0-9][0-9][0-9]");
    var pattern2 = new RegExp("^(0[0-9]|[1-2][0-9])/(0[0-9]|1[0-2])/[1-9][0-9][0-9][0-9]");
    if (value.match(pattern1) || value.match(pattern2)) {
        if (parseInt(value.substr(6, 4)) % 4 != 0 && parseInt(value.substr(3, 2)) == 2 && parseInt(value.substr(0, 2)) == 29) {
            return false;
        } else {
            var dateParts = value.split("/");
            var month = parseInt(dateParts[1]);
            var day = parseInt(dateParts[0]);
            var year = parseInt(dateParts[2]);
            

            if (month < 1 || month > 12)
                return false;
            else if (day < 1 || day > 31)
                return false;
            else if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31)
                return false;
            else if (month == 2) {
                var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
                if (day > 29 || (day == 29 && !isleap))
                    return false;
            }
            return true;
        }
    } else return false;
}


function validateCard(cardTypeObject, cardNumberObject)
{
    var result =  ($("#" + cardNumberObject).validateCreditCard({accept:[$("#"+cardTypeObject).val()]}));
    //return result.luhn_valid;
    return result.valid;
}