﻿using System.Web;
using System.Web.Optimization;

namespace WebImmobilis
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información sobre los formularios. De este modo, estará
            // para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/codebase").Include(
                        "~/Scripts/jquery.validate.min.js",
                        "~/Scripts/jquery.validate.unobtrusive.min.js",
                        "~/Scripts/jquery.unobtrusive-ajax.min.js",
                        "~/Scripts/conditional-validation.js",
                        "~/Scripts/DateComparerValidator.js",
                        "~/Scripts/jquery.numeric.js",
                        "~/Scripts/jquery.alphanum.js",
                        "~/Scripts/numeral.min.js",
                        "~/Scripts/jquery.maxlength.js",
                        "~/Scripts/jquery.Rut.min.js",
                        "~/Scripts/languages.min.js",
                        "~/Scripts/jquery.scrollTo.min.js",
                        "~/Scripts/mvcfoolproof.unobtrusive.min.js",
                        "~/Scripts/scrumit.initialize.js",
                        "~/Scripts/jquery.placeholder.js",
                        "~/Scripts/linq.min.js",
                        "~/Scripts/jquery.scrollTo.min.js",
                         "~/Scripts/jquery.creditCardValidator.js"
                        ));


            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                        "~/Content/vendors/jquery/dist/jquery.min.js",
                      "~/assets/kendo/js/kendo.all.min.js",
                      "~/assets/kendo/js/jszip.min.js",
                      "~/assets/kendo/js/cultures/kendo.culture.es-CL.min.js",
                      "~/assets/kendo/js/kendo.aspnetmvc.min.js",
                      "~/assets/kendo/js/lang/kendo.es-CL.js"
                      ));


            //Intranet
            bundles.Add(new ScriptBundle("~/bundles/intranet/js").Include(
                      "~/Content/vendors/bootstrap/dist/js/bootstrap.bundle.min.js",
                      "~/Content/vendors/fastclick/lib/fastclick.js",
                      "~/Content/vendors/nprogress/nprogress.js",
                      "~/Content/build/js/custom.min.js"
                      ));


            bundles.Add(new StyleBundle("~/Content/Intranet/css").Include(
                        "~/Content/vendors/bootstrap/dist/css/bootstrap.min.css",
                        "~/Content/vendors/font-awesome/css/font-awesome.min.css",
                        "~/Content/vendors/nprogress/nprogress.css",
                        "~/Content/build/css/custom.min.css",
                        "~/Content/Kendo/KendoTheme.css",
                        "~/Content/Kendo/variables.css"
                      ));
            //Intranet

        }
    }
}
