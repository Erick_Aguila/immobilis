﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace WebImmobilis.DAL.Security
{
    public class CustomPrincipal: IPrincipal
    {
        public IIdentity Identity { get; private set; }
        public bool IsInRole(string role)
        {
            if (roles.Any(r => role.Contains(r)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public CustomPrincipal(string Username)
        {
            this.Identity = new GenericIdentity(Username);
        }

        public string UserId { get; set; }
        public int MasterId { get; set; }
        public Guid RoleId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string[] roles { get; set; }
        public bool Recuerdame { get; set; }
        public Guid CotizacionID { get; set; }
        public string CodigoCanal { get; internal set; }
        public int TipoUsuarioId { get; set; }
        public string NombreTipoUsuario { get; set; }
    }

    public class CustomPrincipalSerializeModel
    {
        public string UserId { get; set; }
        public int MasterId { get; set; }
        public Guid RoleId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public Guid CotizacionID { get; set; }
        public string[] roles { get; set; }
        public string CodigoCanal { get; internal set; }
        public bool Recuerdame { get; set; }
        public int TipoUsuarioId { get; set; }
        public string NombreTipoUsuario { get; set; }
    }

}