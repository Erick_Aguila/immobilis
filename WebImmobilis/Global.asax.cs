using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using WebImmobilis.DAL.Security;

namespace WebImmobilis
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authCookie != null)
            {
                try
                {
                    FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                    CustomPrincipalSerializeModel serializeModel = JsonConvert.DeserializeObject<CustomPrincipalSerializeModel>(authTicket.UserData);
                    CustomPrincipal newUser = new CustomPrincipal(authTicket.Name);
                    newUser.UserId = serializeModel.UserId;
                    newUser.MasterId = serializeModel.MasterId;
                    newUser.Name = serializeModel.Name;
                    newUser.Email = serializeModel.Email;
                    newUser.RoleId = serializeModel.RoleId;
                    newUser.CodigoCanal = serializeModel.CodigoCanal;
                    newUser.roles = serializeModel.roles;
                    newUser.TipoUsuarioId = serializeModel.TipoUsuarioId;
                    newUser.NombreTipoUsuario = serializeModel.NombreTipoUsuario;
                    newUser.CotizacionID = Guid.Empty;

                    HttpContext.Current.User = newUser;
                }
                catch (CryptographicException ex)
                {
                    //Logging.Debug(string.Format("Error al obtener usuario conectado {0}", ex.StackTrace));
                    FormsAuthentication.SignOut();
                }
            }

        }

        const int TimedOutExceptionCode = -2147467259;
        public static bool IsMaxRequestExceededException(Exception e)
        {
            Exception main;
            var unhandled = e as HttpUnhandledException;
            if (unhandled != null && unhandled.ErrorCode == TimedOutExceptionCode)
            {
                main = unhandled.InnerException;
            }
            else
            {
                main = e;
            }

            var http = main as HttpException;
            if (http != null && http.ErrorCode == TimedOutExceptionCode)
            {
                if (http.StackTrace.Contains("GetEntireRawContent"))
                {
                    return true;
                }
            }

            return false;
        }

    }
}
