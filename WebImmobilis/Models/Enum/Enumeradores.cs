﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebImmobilis.Models.Enum
{
    public enum EnumTipoUsuario
    {
        Administrador = 1,
        Supervisor = 3,
        Ejecutivo = 4,
        Cliente = 5,
        Propietario = 6
    }
}