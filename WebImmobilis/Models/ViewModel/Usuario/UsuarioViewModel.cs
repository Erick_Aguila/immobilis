﻿using Modelo.TipoUsuario;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebImmobilis.Models.ViewModel.Propiedad;

namespace WebImmobilis.Models.ViewModel.Usuario
{
    public class UsuarioViewModel
    {
        public Guid UsuarioId { get; set; }
        [Required(ErrorMessage = "El campo Rut es obligatorio")]
        [MaxLength(10, ErrorMessage = "El campo Rut tiene un máximo de 10 carácteres.")]
        public string Rut { get; set; }
        [Required(ErrorMessage = "El campo RutDv es obligatorio")]
        public int RutDv { get; set; }
        [Required(ErrorMessage = "El campo Nombre es obligatorio")]
        [MaxLength(50, ErrorMessage = "El campo Nombre tiene un máximo de 50 carácteres.")]
        [Display(Name = "Nombre")]
        [RegularExpression(@"^[a-zA-Z''-'\s]{1,50}$", ErrorMessage = "Carácteres no permitidos.")]
        public string Nombres { get; set; }
        [Required(ErrorMessage = "El campo Apellido Paterno es obligatorio")]
        [MaxLength(50, ErrorMessage = "El campo Apellido Paterno tiene un máximo de 50 carácteres.")]
        [Display(Name = "Apellido Paterno")]
        [RegularExpression(@"^[a-zA-Z''-'\s]{1,50}$", ErrorMessage = "Carácteres no permitidos.")]
        public string ApellidoPaterno { get; set; }
        [Required(ErrorMessage = "El campo Apellido Materno es obligatorio")]
        [MaxLength(50, ErrorMessage = "El campo Apellido Materno tiene un máximo de 50 carácteres.")]
        [Display(Name = "Apellido Materno")]
        [RegularExpression(@"^[a-zA-Z''-'\s]{1,50}$", ErrorMessage = "Carácteres no permitidos.")]
        public string ApellidoMaterno { get; set; }
        [Required(ErrorMessage = "El campo Email es obligatorio")]
        [MaxLength(100, ErrorMessage = "El campo Email tiene un máximo de 100 carácteres.")]
        [EmailAddress(ErrorMessage = "Email no válido.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "El campo Clave es obligatorio")]
        [MaxLength(100, ErrorMessage = "El campo Clave tiene un máximo de 100 carácteres.")]
        public string Clave { get; set; }
        [Required(ErrorMessage = "El campo Celular es obligatorio")]
        [MaxLength(10, ErrorMessage = "El campo Celular tiene un máximo de 10 carácteres.")]
        public string Celular { get; set; }
        [Required(ErrorMessage = "El campo Tipo Usuario Id es obligatorio")]
        [Display(Name = "Tipo de Usuario")]
        public int TipoUsuarioId { get; set; }
        public string NombreVista { get; set; }

        public string NombreCompleto {
            get {
                return string.Format("{0} {1} {2}", Nombres, ApellidoPaterno, ApellidoMaterno);
            }
        }
        public List<TipoUsuarioModel> TipoUsuarioList { get; set; }
        public TipoUsuarioModel TipoUsuario { get; set; }
        public List<DepartamentoViewModel> ListaDepartamentosEnReserva { get; set; }
        public List<DepartamentoViewModel> ListaDepartamentosComprados { get; set; }

    }
}