﻿using Modelo.Banco;
using Modelo.Propiedad;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebImmobilis.Models.ViewModel.Reservar
{
    public class ReservaViewModel
    {
        [Required(ErrorMessage = "El campo Banco id es obligatorio.")]
        public int BancoId { get; set; }
        [Required(ErrorMessage = "El campo Nombre Banco id es obligatorio.")]
        public string NombreBanco { get; set; }
        [Required(ErrorMessage = "El campo Rut es obligatorio.")]
        public string Rut { get; set; }
        [Required(ErrorMessage = "El campo Rut Dv es obligatorio.")]
        public int RutDv { get; set; }
        [Required(ErrorMessage = "El campo Monto pie es obligatorio.")]
        public string MontoPie { get; set; }
        [Required(ErrorMessage = "El campo Clave es obligatorio.")]
        public string Clave { get; set; }
        public Guid PropiedadId { get; set; }
        public PropiedadModel Propiedad { get; set; }

        public List<BancoModel> BancoList { get; set; }

    }
}