﻿using Modelo.Banco;
using Modelo.Comuna;
using Modelo.Departamento;
using Modelo.EstadoPropiedad;
using Modelo.Region;
using Modelo.TipoPropiedad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebImmobilis.Models.ViewModel.Propiedad
{
    public class PropiedadViewModel
    {
        public Guid PropiedadId { get; set; }
        public string Direccion { get; set; }
        public int EstadoPropiedadId { get; set; }
        public int TipoPropiedadId { get; set; }
        public int Tasacion { get; set; }
        public string Notaria { get; set; }
        public string ConservadorBienes { get; set; }
        public int GiroNegocioId { get; set; }
        public int RegionId { get; set; }
        public int ComunaId { get; set; }
        public int PrecioUF { get; set; }
        public bool EsPropiedadDeTercero { get; set; }
        public int BancoId { get; set; }
        public bool UtilizaCreditoHipotecario { get; set; }
        public Guid DepartamentoId { get; set; }
        public Guid CasaId { get; set; }
        public Guid BodegaId { get; set; }
        public Guid EstacionamientoId { get; set; }
        public Guid OficinaId { get; set; }
        public Guid UsuarioId { get; set; }


        public EstadoPropiedadModel EstadoPropiedad { get; set; }
        public TipoPropiedadModel TipoPropiedad { get; set; }
        public RegionModel Region { get; set; }
        public ComunaModel Comuna { get; set; }
        public BancoModel Banco { get; set; }

        public DepartamentoModel Departamento { get; set; }

        //Listas
        public List<TipoPropiedadModel> TipoPropiedadList { get; set; }
        public List<RegionModel> RegionList { get; set; }
        public List<ComunaModel> ComunaList { get; set; }
        public List<BancoModel> BancoList { get; set; }

    }
}