﻿using Modelo.Banco;
using Modelo.Comuna;
using Modelo.Region;
using Modelo.TipoPropiedad;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebImmobilis.Models.ViewModel.Propiedad
{
    public class BasePropiedadViewModel
    {
        public Guid PropiedadId { get; set; }
        [Required(ErrorMessage = "El campo Dirección es requerido")]
        [MaxLength(100, ErrorMessage = "El campo Dirección debe tener un máximo de 100 carácteres")]
        [MinLength(2, ErrorMessage = "El campo Dirección debe tener al menos 2 carácteres")]
        [Display(Name = "Dirección")]
        public string Direccion { get; set; }
        [Required(ErrorMessage = "El campo Estado Propiedad Id es requerido")]
        public int EstadoPropiedadId { get; set; }
        [Required(ErrorMessage = "El campo Tipo propiedad Id es requerido")]
        [Display(Name = "Tipo propiedad")]
        public int TipoPropiedadId { get; set; }
        [Display(Name = "Tasación")]
        public int Tasacion { get; set; }
        public string Notaria { get; set; }
        [Display(Name = "Conservador de bienes raizes")]
        public string ConservadorBienes { get; set; }
        [Display(Name = "Giro Negocio")]
        public int GiroNegocioId { get; set; }
        [Required(ErrorMessage = "El campo Región es requerido")]
        [Display(Name = "Región")]
        public int RegionId { get; set; }
        [Required(ErrorMessage = "El campo Comuna es requerido")]
        [Display(Name = "Comuna")]
        public int ComunaId { get; set; }
        [Required(ErrorMessage = "El campo Precio UF es obligatorio.")]
        [Display(Name = "Precio UF")]
        [Range(100,10000,ErrorMessage = "El precio UF debe estar entre los 100 y 10.000")]
        public int PrecioUF { get; set; }
        public bool EsPropiedadDeTercero { get; set; }
        [Display(Name = "Banco")]
        public int BancoId { get; set; }
        public bool UtilizaCreditoHipotecario { get; set; }
        public Guid UsuarioId { get; set; }


        public List<TipoPropiedadModel> TipoPropiedadList { get; set; }
        public List<RegionModel> RegionList { get; set; }
        public List<ComunaModel> ComunaList { get; set; }
        public List<BancoModel> BancoList { get; set; }
    }
}