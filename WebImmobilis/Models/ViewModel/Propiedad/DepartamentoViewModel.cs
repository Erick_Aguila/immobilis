﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebImmobilis.Models.ViewModel.Propiedad
{
    public class DepartamentoViewModel : BasePropiedadViewModel
    {
        public Guid DepartamentoId { get; set; }
        [Required(ErrorMessage = "El campo Nombre Edificio es requerido")]
        [MaxLength(50, ErrorMessage = "El campo Nombre Edificio debe tener un máximo de 50 carácteres.")]
        [MinLength(2, ErrorMessage = "El campo Nombre Edificio debe tener al menos 2 carácteres")]
        [Display(Name = "Nombre Edificio")]
        public string NombreEdificio { get; set; }
        [Required(ErrorMessage = "El campo Cantidad de Dormitorios es Requerido")]
        [Display(Name = "Cantidad de dormitorios")]
        [Range(1,99,ErrorMessage = "La cantidad de dormitorio debe estar entre 1 a 99.")]
        public int cantidadDormitorios { get; set; }
        [Required(ErrorMessage = "El campo Cantidad de Baños es Requerido")]
        [Display(Name = "Cantidad de Baños")]
        [Range(1,99,ErrorMessage = "La cantidad de baños debe estar entre 1 y 99.")]
        public int cantidadBanios { get; set; }
        [Required(ErrorMessage = "El campo Metros cuadrados es Requerido")]
        [Display(Name = "Metros cuadrados")]
        [Range(40,1000, ErrorMessage = "Los metros cuadrado debe estar entre 40 y 1.000")]
        public float MetrosCuadrados { get; set; }
        public bool Activo { get; set; }
        public DateTime FechaCreacion { get; set; }
    }
}