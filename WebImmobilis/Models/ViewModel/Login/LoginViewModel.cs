﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebImmobilis.Models.ViewModel.Login
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "El campo Email es obligatorio.")]
        [MaxLength(100, ErrorMessage = "El campo Email tiene un máximo de 100 carácateres")]
        [MinLength(2, ErrorMessage = "El campo Email debe tener al meno 2 carácteres")]
        [EmailAddress(ErrorMessage = "El correo electrónico es incorrecto")]
        public string Email { get; set; }
        [Required(ErrorMessage = "El campo Clave es obligatorio.")]
        [MaxLength(100, ErrorMessage = "El campo Clave tiene un máximo de 100 carácateres")]
        [MinLength(2,ErrorMessage = "El campo Clave debe tener al meno 2 carácteres")]
        public string Clave { get; set; }
        public bool Recuerdame { get; set; }
        public bool AutenticadoOK { get; set; }
    }
}