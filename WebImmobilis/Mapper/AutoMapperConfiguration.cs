﻿using AutoMapper;
using Modelo.Departamento;
using Modelo.Propiedad;
using Modelo.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebImmobilis.Models.ViewModel.Propiedad;
using WebImmobilis.Models.ViewModel.Usuario;

namespace WebImmobilis.Mapper
{
    public class AutoMapperConfiguration
    {
        static IMapper instance = null;
        public static IMapper RegisterAutoMapper()
        {
            if (instance != null)
            {
                return instance;
            }

            MapperConfiguration mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UsuarioViewModel, UsuarioModel>().ReverseMap();
                cfg.CreateMap<DepartamentoViewModel, DepartamentoModel>().ReverseMap();
                cfg.CreateMap<DepartamentoViewModel, PropiedadModel>().ReverseMap();
            });

            instance = mapperConfiguration.CreateMapper();
            return instance;

        }
    }
}