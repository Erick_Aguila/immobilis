﻿using Modelo.TipoUsuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dato.TipoUsuarioDao
{
    public class TipoUsuarioDao
    {
        DB_A645C6_ImmobilisEntities entities = new DB_A645C6_ImmobilisEntities();


        public TipoUsuarioModel Leer(TipoUsuarioModel tipoUsuarioModel)
        {
            try
            {
                var tipoUsuario = entities.TipoUsuario.Where(c => c.TipoUsuarioId == tipoUsuarioModel.TipoUsuarioId).FirstOrDefault();
                if (tipoUsuario != null)
                {
                    tipoUsuarioModel.Nombre = tipoUsuario.Nombre;
                    tipoUsuarioModel.Activo = tipoUsuario.Activo;
                    return tipoUsuarioModel;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

        public List<TipoUsuarioModel> Listar()
        {
            List<TipoUsuarioModel> tipoUsuarioList = new List<TipoUsuarioModel>();
            try
            {
                var lista = entities.TipoUsuario.ToList();
                if (lista.Count > 0 && lista != null)
                {
                    foreach (var tipoUsuario in lista)
                    {
                        tipoUsuarioList.Add(new TipoUsuarioModel() { TipoUsuarioId = tipoUsuario.TipoUsuarioId, Nombre = tipoUsuario.Nombre, Activo = tipoUsuario.Activo});
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return tipoUsuarioList;
        }
    }
}
