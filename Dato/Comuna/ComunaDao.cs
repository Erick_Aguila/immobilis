﻿using AutoMapper;
using Dato.Mapper;
using Modelo.Comuna;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dato.ComunaDao
{
    public class ComunaDao
    {
        DB_A645C6_ImmobilisEntities entities = new DB_A645C6_ImmobilisEntities();
        IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();

        public ComunaModel Crear(ComunaModel comunaModel)
        {
            try
            {
                var comuna = mapper.Map<Comuna>(comunaModel);
                entities.Comuna.Add(comuna);
                var respuesta = entities.SaveChanges();
                if (respuesta != 1)
                {
                    comunaModel.Error = -1;
                    comunaModel.Mensaje = "Hubo un problema al crear una Comuna";
                }
            }
            catch (Exception ex)
            {
                comunaModel.Error = -1;
                comunaModel.Mensaje = ex.Message;
            }
            return comunaModel;
        }

        public bool Actualizar(ComunaModel comunaModel)
        {
            try
            {
                var comuna = mapper.Map<Comuna>(comunaModel);
                entities.Entry(comuna).State = System.Data.Entity.EntityState.Modified;
                var respuesta = entities.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
            return false;
        }

        public ComunaModel Leer(int comunaId)
        {
            ComunaModel comunaModel = new ComunaModel();
            try
            {
                var comuna = entities.Comuna.Where(c => c.ComunaId == comunaId).FirstOrDefault();
                if (comuna != null)
                {
                    comunaModel = mapper.Map<ComunaModel>(comuna);
                }
            }
            catch (Exception ex)
            {
                comunaModel.Error = -1;
                comunaModel.Mensaje = ex.Message;
            }
            return comunaModel;
        }

        public List<ComunaModel> ObtenerComunaPorRegion(int regionId)
        {
            List<ComunaModel> comunaList = new List<ComunaModel>();
            try
            {
                var lista = entities.Comuna.Where(c => c.RegionId == regionId).ToList();
                if (lista !=null && lista.Count() > 0)
                {
                    comunaList = mapper.Map<List<ComunaModel>>(lista);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return comunaList;
        }

        public List<ComunaModel> Listar()
        {
            List<ComunaModel> comunaList = new List<ComunaModel>();
            try
            {
                var lista = entities.Comuna.ToList();
                if (lista != null && lista.Count() > 0)
                {
                    comunaList = mapper.Map<List<ComunaModel>>(lista);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return comunaList;
        }

    }
}
