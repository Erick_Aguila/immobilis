﻿using AutoMapper;
using Modelo.Agenda;
using Modelo.Archivo;
using Modelo.Banco;
using Modelo.Ciudad;
using Modelo.Comuna;
using Modelo.Contrato;
using Modelo.Departamento;
using Modelo.EstadoPropiedad;
using Modelo.GiroNegocio;
using Modelo.Pago;
using Modelo.Propiedad;
using Modelo.Publicacion;
using Modelo.Region;
using Modelo.Reserva;
using Modelo.TipoCuenta;
using Modelo.TipoPropiedad;
using Modelo.TipoUsuario;
using Modelo.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dato.Mapper
{
    public class AutoMapperConfiguration
    {
        static IMapper instance = null;
        public static IMapper RegisterAutoMapper()
        {
            if (instance != null)
            {
                return instance;
            }

            MapperConfiguration mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AgendaModel, Agenda>().ReverseMap();
                cfg.CreateMap<ArchivoModel, Archivo>().ReverseMap();
                cfg.CreateMap<BancoModel, Banco>().ReverseMap();
                cfg.CreateMap<ContratoModel, Contrato>().ReverseMap();
                cfg.CreateMap<EstadoPropiedadModel, EstadoPropiedad>().ReverseMap();
                cfg.CreateMap<GiroNegocioModel, GiroNegocio>().ReverseMap();
                cfg.CreateMap<PagoModel, Pago>().ReverseMap();
                cfg.CreateMap<PropiedadModel, Propiedad>().ReverseMap();
                cfg.CreateMap<PublicacionModel, Publicacion>().ReverseMap();
                cfg.CreateMap<RegionModel, Region>().ReverseMap();
                cfg.CreateMap<ReservaModel, Reserva>().ReverseMap();
                cfg.CreateMap<TipoCuentaModel, TipoCuenta>().ReverseMap();
                cfg.CreateMap<TipoPropiedadModel, TipoPropiedad>().ReverseMap();
                cfg.CreateMap<TipoUsuarioModel, TipoUsuario>().ReverseMap();
                cfg.CreateMap<UsuarioModel, Usuario>().ReverseMap();
                cfg.CreateMap<ComunaModel, Comuna>().ReverseMap();
                cfg.CreateMap<CiudadModel, Ciudad>().ReverseMap();
                cfg.CreateMap<DepartamentoModel, Departamento>().ReverseMap();

            });

            instance = mapperConfiguration.CreateMapper();
            return instance;

        }
    }
}
