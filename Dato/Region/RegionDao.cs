﻿using AutoMapper;
using Dato.Mapper;
using Modelo.Region;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dato.RegionDao
{
    public class RegionDao
    {
        DB_A645C6_ImmobilisEntities entities = new DB_A645C6_ImmobilisEntities();
        IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();

        public RegionModel Crear(RegionModel regionModel)
        {
            try
            {
                var region = mapper.Map<Region>(regionModel);
                entities.Region.Add(region);
                var respuesta = entities.SaveChanges();
                if (respuesta != 1)
                {
                    regionModel.Error = -1;
                    regionModel.Mensaje = "Hubo un problema al crear una nueva Región";
                }
            }
            catch (Exception ex)
            {
                regionModel.Error = -1;
                regionModel.Mensaje = ex.Message;
            }
            return regionModel;
        }

        public bool Actualizar(RegionModel regionModel)
        {
            try
            {
                var region = mapper.Map<Region>(regionModel);
                entities.Entry(region).State = System.Data.Entity.EntityState.Modified;
                var resp = entities.SaveChanges();
                if (resp != 1)
                {
                    regionModel.Error = -1;
                    regionModel.Mensaje = "HUbo un problema al actualizar la región";
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
            return false;
        }

        public RegionModel Leer(int regionId)
        {
            RegionModel regionModel = new RegionModel();
            try
            {
                var region = entities.Region.Where(c => c.RegionId == regionId).FirstOrDefault();
                regionModel = mapper.Map<RegionModel>(region);
            }
            catch (Exception ex)
            {
                regionModel.Error = -1;
                regionModel.Mensaje = ex.Message;
            }
            return regionModel;
        }

        public List<RegionModel> Listar()
        {
            List<RegionModel> lista = new List<RegionModel>();
            try
            {
                var regionList = entities.Region.ToList();
                lista = mapper.Map<List<RegionModel>>(regionList);
            }
            catch (Exception ex)
            {
                throw;
            }
            return lista;
        }

    }
}
