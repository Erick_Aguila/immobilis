﻿using AutoMapper;
using Dato.Mapper;
using Modelo.Departamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dato.DepartamentoDao
{
    public class DepartamentoDao
    {
        DB_A645C6_ImmobilisEntities entities = new DB_A645C6_ImmobilisEntities();
        IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();

        public DepartamentoModel Crear(DepartamentoModel departamentoModel)
        {
            try
            {
                var departamento = mapper.Map<Departamento>(departamentoModel);
                entities.Departamento.Add(departamento);
                var resp = entities.SaveChanges();
                if (resp != 1)
                {
                    departamentoModel.Error = -1;
                    departamentoModel.Mensaje = "Hubo un problema al crea el departamento";
                }
            }
            catch (Exception ex)
            {
                departamentoModel.Error = -1;
                departamentoModel.Mensaje = ex.Message;
            }
            return departamentoModel;
        }

        public bool Actualizar(DepartamentoModel departamentoModel)
        {
            bool respuesta = false;
            try
            {
                var departamento = mapper.Map<Departamento>(departamentoModel);
                entities.Entry(departamento).State = System.Data.Entity.EntityState.Modified;
                var resp = entities.SaveChanges();
                respuesta = true;
            }
            catch (Exception ex)
            {
                departamentoModel.Error = -1;
                departamentoModel.Mensaje = ex.Message;
            }
            return respuesta;
        }

        public DepartamentoModel Leer(Guid departamentoId)
        {
            DepartamentoModel departamentoModel = new DepartamentoModel();
            try 
            {
                var departamento = entities.Departamento.Where(c => c.DepartamentoId == departamentoId).FirstOrDefault();
                departamentoModel = mapper.Map<DepartamentoModel>(departamento);
            }
            catch (Exception ex)
            {
                departamentoModel.Error = -1;
                departamentoModel.Mensaje = ex.Message;
            }
            return departamentoModel;
        }


    }
}
