﻿using AutoMapper;
using Dato.Mapper;
using Modelo.EstadoPropiedad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dato.EstadoPropiedadDao
{
    public class EstadoPropiedadDao
    {
        DB_A645C6_ImmobilisEntities entities = new DB_A645C6_ImmobilisEntities();
        IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();

        public EstadoPropiedadModel Crear(EstadoPropiedadModel estadoPropiedadModel)
        {
            try
            {
                var estadoPropiedad = mapper.Map<EstadoPropiedad>(estadoPropiedadModel);
                var estadoPropiedadCreado = entities.EstadoPropiedad.Add(estadoPropiedad);
                var respuesta = entities.SaveChanges();
                if (respuesta != 1)
                {
                    estadoPropiedadModel.Error = -1;
                    estadoPropiedadModel.Mensaje = "Hubo un problema al crear el registro de estado propiedad.";
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return estadoPropiedadModel;
        }


        public bool Actualizar(EstadoPropiedadModel estadoPropiedadModel)
        {
            bool respuesta = false;
            try
            {
                using (DB_A645C6_ImmobilisEntities entities = new DB_A645C6_ImmobilisEntities())
                {
                    var estadoPropiedad = entities.EstadoPropiedad.Where(c => c.EstadoPropiedadId == estadoPropiedadModel.EstadoPropiedadId).FirstOrDefault();
                    if (estadoPropiedad != null)
                    {
                        var estadoProp = mapper.Map<EstadoPropiedad>(estadoPropiedadModel);
                        entities.Entry(estadoProp).State = System.Data.Entity.EntityState.Modified;
                        var resp = entities.SaveChanges();
                        respuesta = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return respuesta;
        }

        public EstadoPropiedadModel Leer(int estadoPropiedadId)
        {
            EstadoPropiedadModel estadoPropiedadModel = new EstadoPropiedadModel();
            try
            {
                var estadoPropiedad = entities.EstadoPropiedad.Where(c => c.EstadoPropiedadId == estadoPropiedadId).FirstOrDefault();
                if (estadoPropiedad != null)
                {
                    estadoPropiedadModel = mapper.Map<EstadoPropiedadModel>(estadoPropiedad);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return estadoPropiedadModel;
        }

        public bool Eliminar(int estadoPropiedadId)
        {
            bool respuesta = false;
            try
            {
                var estadoPropiedad = entities.EstadoPropiedad.Where(c => c.EstadoPropiedadId == estadoPropiedadId).FirstOrDefault();
                if (estadoPropiedad != null)
                {
                    entities.EstadoPropiedad.Remove(estadoPropiedad);
                    entities.SaveChanges();
                    respuesta = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return respuesta;
        }

        public List<EstadoPropiedadModel> ListarTodo()
        {
            try
            {
                var lista = entities.EstadoPropiedad.ToList();
                if (lista != null)
                {
                    return mapper.Map<List<EstadoPropiedadModel>>(lista);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }


    }
}
