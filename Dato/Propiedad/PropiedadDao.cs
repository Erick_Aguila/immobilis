﻿using AutoMapper;
using Dato.Mapper;
using Modelo.Propiedad;
using Modelo.TipoPropiedad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dato.PropiedadDao
{
    public class PropiedadDao
    {
        DB_A645C6_ImmobilisEntities entities = new DB_A645C6_ImmobilisEntities();
        IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();

        public PropiedadModel Crear(PropiedadModel propiedadModel)
        {
            try
            {
                var propiedad = mapper.Map<Propiedad>(propiedadModel);
                entities.Propiedad.Add(propiedad);
                var resp = entities.SaveChanges();
                if (resp != 1)
                {
                    propiedadModel.Error = -1;
                    propiedadModel.Mensaje = "Hubo un problema al crear la propiedad";
                }
            }
            catch (Exception ex)
            {
                propiedadModel.Error = -1;
                propiedadModel.Mensaje = ex.Message;
            }
            return propiedadModel;
        }

        public bool Actualizar(PropiedadModel propiedadModel)
        {
            var respuesta = false;
            try
            {
                var propiedad = mapper.Map<Propiedad>(propiedadModel);
                entities.Entry(propiedad).State = System.Data.Entity.EntityState.Modified;
                var resp = entities.SaveChanges();
                respuesta = true;
            }
            catch (Exception ex)
            {
                throw;
            }
            return respuesta;
        }

        public PropiedadModel Leer(Guid propiedadId)
        {
            PropiedadModel propiedadModel = new PropiedadModel();
            try
            {
                var propiedad = entities.Propiedad.Where(c => c.PropiedadId == propiedadId).FirstOrDefault();
                propiedadModel = mapper.Map<PropiedadModel>(propiedad);
            }
            catch (Exception ex)
            {
                propiedadModel.Error = -1;
                propiedadModel.Mensaje = ex.Message;
            }
            return propiedadModel;
        }

        public List<PropiedadModel> ListarTodo()
        {
            List<PropiedadModel> propiedadList = new List<PropiedadModel>();
            try
            {
                var listaPropiedad = entities.Propiedad.ToList();
                propiedadList = mapper.Map<List<PropiedadModel>>(listaPropiedad);
            }
            catch (Exception ex)
            {
                throw;
            }
            return propiedadList;
        }

        public bool ReservarPropiedad(PropiedadModel propiedadModel)
        {
            try
            {
                using (DB_A645C6_ImmobilisEntities entities = new DB_A645C6_ImmobilisEntities())
                {
                    var propiedad = entities.Propiedad.Where(c => c.PropiedadId == propiedadModel.PropiedadId).FirstOrDefault();
                    if (propiedad != null)
                    {
                        //cambia de estado a reservado
                        propiedad.EstadoPropiedadId = 3;
                        propiedad.UsuarioId = propiedadModel.UsuarioId;

                        //actualizamos la propiedad
                        entities.Entry(propiedad).State = System.Data.Entity.EntityState.Modified;
                        var resp = entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return false;
        }

        public PropiedadDisponibleModel ListarPropiedadDisponible()
        {
            PropiedadDisponibleModel propiedadDisponibleModel = new PropiedadDisponibleModel();
            try
            {
                //Obtenenermos una lista de departamentos
                var departamentoList = entities.Propiedad.Where(c => c.EstadoPropiedadId == 1 && c.DepartamentoId != null).ToList();
                var listaDepartamento = mapper.Map<List<PropiedadModel>>(departamentoList);
                propiedadDisponibleModel.ListaDepartamentos = listaDepartamento;

                //Obtener lista de Casas
                //Obtener lista de Estacionamientos
            }
            catch (Exception ex)
            {
                propiedadDisponibleModel.Error = -1;
                propiedadDisponibleModel.Mensaje = ex.Message;
            }
            return propiedadDisponibleModel;
        }


        public List<PropiedadModel> ListarPropiedadPorEstado(int estadoPropiedadId, Guid usuarioId)
        {
            List<PropiedadModel> propiedadList = new List<PropiedadModel>();
            try
            {
                var lista = entities.Propiedad.Where(c => c.EstadoPropiedadId == estadoPropiedadId && c.UsuarioId == usuarioId).ToList();
                propiedadList = mapper.Map<List<PropiedadModel>>(lista);
            }
            catch (Exception ex)
            {
                throw;
            }
            return propiedadList;
        }

        public List<PropiedadModel> ListarDepartamentosReservados()
        {
            List<PropiedadModel> listaPropiedad = new List<PropiedadModel>();
            try
            {
                var lista = entities.Propiedad.Where(c => c.EstadoPropiedadId == 3 && c.DepartamentoId != null).ToList();
                listaPropiedad = mapper.Map<List<PropiedadModel>>(lista);
            }
            catch (Exception ex)
            {
                throw;
            }
            return listaPropiedad;
        }

    }
}
