﻿using AutoMapper;
using Dato.Mapper;
using Modelo.Agenda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dato.AgendaDao
{
    public class AgendaDao
    {
        DB_A645C6_ImmobilisEntities entities = new DB_A645C6_ImmobilisEntities();
        IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();

        /// <summary>
        /// Método para crear una agenda
        /// </summary>
        /// <param name="agendaModel">Modelo que contien los datos necesario para crear una agenda</param>
        /// <returns></returns>
        public AgendaModel Crear(AgendaModel agendaModel)
        {
            try
            {
                var agenda = mapper.Map<Agenda>(agendaModel);
                entities.Agenda.Add(agenda);
                var respesta = entities.SaveChanges();
                if (respesta != 1)
                {
                    agendaModel.Error = -1;
                    agendaModel.Mensaje = "Hubo un problema al crear una nueva agenda";
                }
                else
                {
                    agendaModel = mapper.Map<AgendaModel>(agenda);
                }
            }
            catch (Exception ex)
            {
                agendaModel.Error = 500;
                agendaModel.Mensaje = "Hubo un problema interno al crear una nueva agenda";
            }
            return agendaModel;
        }

        public bool Actualizar(AgendaModel agendaModel)
        {
            bool respuesta = false;
            try
            {
                Agenda agenda = new Agenda()
                {
                    FechaAgenda = agendaModel.FechaAgenda,
                    FechaCreacion = agendaModel.FechaCreacion,
                    ReservaId = agendaModel.ReservaId
                };

                //var respuesta = entities.Agenda.
            }
            catch (Exception ex)
            {
                throw;
            }
            return respuesta;
        }

        public AgendaModel Leer(AgendaModel agendaModel)
        {
            try
            {
                var respuesta = entities.Agenda.Where(c => c.AgendaId == agendaModel.AgendaId).FirstOrDefault();
                if (respuesta != null)
                {
                    agendaModel.FechaAgenda = respuesta.FechaAgenda;
                    agendaModel.FechaCreacion = respuesta.FechaCreacion;
                    agendaModel.ReservaId = respuesta.ReservaId;
                    return agendaModel;
                }
            }
            catch (Exception ex)
            {   
                throw;
            }
            return null;
        }

        public bool Eliminar(int agendaId)
        {
            bool respuesta = false;
            try
            {
                var agenda = entities.Agenda.Where(c => c.AgendaId == agendaId).FirstOrDefault();
                if (agenda != null)
                {
                    entities.Agenda.Remove(agenda);
                    entities.SaveChanges();
                    respuesta = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return respuesta;
        }


        public List<AgendaModel> Listar()
        {
            List<AgendaModel> agendaList = new List<AgendaModel>();
            try
            {
                var lista = entities.Agenda.ToList();
                if (lista.Count > 0 && lista != null)
                {
                    foreach (var agenda in lista)
                    {
                        agendaList.Add(new AgendaModel() { AgendaId = agenda.AgendaId, FechaAgenda = agenda.FechaAgenda, FechaCreacion = agenda.FechaCreacion, ReservaId = agenda.ReservaId });
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return agendaList;
        }

    }
}
