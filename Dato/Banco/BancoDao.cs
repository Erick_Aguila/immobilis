﻿using AutoMapper;
using Dato.Mapper;
using Modelo.Banco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dato.BancoDao
{
    public class BancoDao
    {
        DB_A645C6_ImmobilisEntities entities = new DB_A645C6_ImmobilisEntities();
        IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();

        public BancoModel Crear(BancoModel bancoModel)
        {
            try
            {
                var banco = mapper.Map<Banco>(bancoModel);
                var bancoCreado = entities.Banco.Add(banco);
                var response = entities.SaveChanges();
                if (response != 1)
                {
                    bancoModel.Error = -1;
                    bancoModel.Mensaje = "Hubo un problema al crear el banco.";
                }
            }
            catch (Exception ex)
            {
                bancoModel.Error = -1;
                bancoModel.Mensaje = ex.Message;
            }
            return bancoModel;
        }

        public bool Actualizar(BancoModel bancoModel)
        {
            try
            {
                var banco = mapper.Map<Banco>(bancoModel);
                entities.Entry(banco).State = System.Data.Entity.EntityState.Modified;
                var respuesta = entities.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
            return false;
        }

        public BancoModel Leer(int bancoId)
        {
            BancoModel bancoModel = new BancoModel();
            try
            {
                var banco = entities.Banco.Where(c => c.BancoId == bancoId).FirstOrDefault();
                if (banco != null)
                {
                    bancoModel = mapper.Map<BancoModel>(banco);
                }
            }
            catch (Exception ex)
            {
                bancoModel.Error = -1;
                bancoModel.Mensaje = ex.Message;
            }
            return bancoModel;
        }


        public List<BancoModel> Listar()
        {
            List<BancoModel> bancoList = new List<BancoModel>();
            try
            {
                var lista = entities.Banco.ToList();
                if (lista.Count > 0 && lista != null)
                {
                    bancoList = mapper.Map<List<BancoModel>>(lista);
                    //List<BancoModel> bancoList = new List<BancoModel>();
                    //foreach (var banco in lista)
                    //{
                    //    bancoList.Add(new BancoModel() { BancoId = banco.BancoId, NombreBanco = banco.NombreBanco, Activo = banco.Activo });
                    //}
                    //return bancoList;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return bancoList;
        }
    }
}
