﻿using AutoMapper;
using Dato.Mapper;
using Modelo.TipoUsuario;
using Modelo.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dato.UsuarioDao
{
    public class UsuarioDao
    {
        /// <summary>
        /// Crear usuario para el sistema, puede ser cliente o funcionario
        /// </summary>
        /// <param name="usuarioModel"></param>
        /// <returns></returns>
        public UsuarioModel Crear(UsuarioModel usuarioModel)
        {
            try
            {
                using (DB_A645C6_ImmobilisEntities entities = new DB_A645C6_ImmobilisEntities())
                {
                    //Validamos si el rut del usuario ya está registrado
                    var existeUsuario = entities.Usuario.Where(c => c.Rut == usuarioModel.Rut).FirstOrDefault();
                    var existeEmail = entities.Usuario.Where(c => c.Email == usuarioModel.Email).FirstOrDefault();
                    if (existeUsuario == null && existeEmail == null)
                    {
                        IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();
                        var usuario = mapper.Map<Usuario>(usuarioModel);
                        var usuarioCreado = entities.Usuario.Add(usuario);
                        var response = entities.SaveChanges();
                        if (response != 1)
                        {
                            usuarioModel.Error = -1;
                            usuarioModel.Mensaje = "Hubo un problema al insertar el usuario, vuelva a intentar más tarde.";
                        }
                    }
                    else
                    {
                        usuarioModel.Error = -1;
                        usuarioModel.Mensaje = "Usuario ya registrado.";
                    }
                }
            }
            catch (Exception ex)
            {
                usuarioModel.Error = -1;
                usuarioModel.Mensaje = ex.Message;
            }
            return usuarioModel;
        }

        public bool Actualizar(UsuarioModel usuarioModel)
        {
            try
            {
                IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();
                var usuario = mapper.Map<Usuario>(usuarioModel);

                using (DB_A645C6_ImmobilisEntities entities = new DB_A645C6_ImmobilisEntities())
                {
                    entities.Entry(usuario).State = System.Data.Entity.EntityState.Modified;
                    var resp = entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                usuarioModel.Error = -1;
                usuarioModel.Mensaje = ex.Message;
            }
            return false;
        }

        public UsuarioModel Leer(UsuarioModel usuarioModel)
        {
            try
            {
                using (DB_A645C6_ImmobilisEntities entities = new DB_A645C6_ImmobilisEntities())
                {
                    Usuario usuario = entities.Usuario.Where(c => c.UsuarioId == usuarioModel.UsuarioId).FirstOrDefault();
                    if (usuario != null)
                    {
                        IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();
                        usuarioModel = mapper.Map<UsuarioModel>(usuario);
                        return usuarioModel;
                    }
                }
            }
            catch (Exception ex)
            {
                usuarioModel.Error = -1;
                usuarioModel.Mensaje = ex.Message;
            }
            return usuarioModel;
        }

        public List<UsuarioModel> Listar()
        {
            List<UsuarioModel> usuarioList = new List<UsuarioModel>();
            try
            {
                using (DB_A645C6_ImmobilisEntities entities = new DB_A645C6_ImmobilisEntities())
                {
                    var lista = entities.Usuario.ToList().OrderBy(c => c.Nombres);
                    if (lista.Count() > 0 && lista != null)
                    {
                        IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();
                        foreach (var usuario in lista)
                        {
                            var usuarioModel = mapper.Map<UsuarioModel>(usuario);
                            var tipoUsuario = entities.TipoUsuario.Where(c => c.TipoUsuarioId == usuarioModel.TipoUsuarioId).FirstOrDefault();
                            usuarioModel.TipoUsuario = mapper.Map<TipoUsuarioModel>(tipoUsuario);
                            usuarioList.Add(usuarioModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //usuarioList.Error = new Modelo.Base.BaseModel.ErrorTo() { Codigo = "500", Mensaje = ex.Message };
            }
            return usuarioList;
        }

        /// <summary>
        /// Método para autenticar usuario
        /// </summary>
        /// <param name="usuarioModel"></param>
        /// <returns></returns>
        public UsuarioModel Autenticar(UsuarioModel usuarioModel)
        {
            try
            {
                using (DB_A645C6_ImmobilisEntities entities = new DB_A645C6_ImmobilisEntities())
                {
                    var UsuarioAutenticado = entities.Usuario.Where(c => c.Email == usuarioModel.Email && c.Clave == usuarioModel.Clave).FirstOrDefault();
                    IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();
                    usuarioModel = mapper.Map<UsuarioModel>(UsuarioAutenticado);
                }
            }
            catch (Exception ex)
            {
                usuarioModel.Error = -1;
                usuarioModel.Mensaje = ex.Message;
            }
            return usuarioModel;
        }


    }
}
