﻿using AutoMapper;
using Dato.Mapper;
using Modelo.TipoPropiedad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dato.TipoPropiedadDao
{
    public class TipoPropiedadDao
    {
        DB_A645C6_ImmobilisEntities entities = new DB_A645C6_ImmobilisEntities();
        IMapper mapper = AutoMapperConfiguration.RegisterAutoMapper();

        public TipoPropiedadModel Crear(TipoPropiedadModel tipoPropiedadModel)
        {
            try
            {
                var tipoPropiedad = mapper.Map<TipoPropiedad>(tipoPropiedadModel);
                entities.TipoPropiedad.Add(tipoPropiedad);
                var respuesta = entities.SaveChanges();
                if (respuesta != 1)
                {
                    tipoPropiedadModel.Error = -1;
                    tipoPropiedadModel.Mensaje = "Hubo un problema al crear el registro de Tipo propiedad. ";
                }
            }
            catch (Exception ex)
            {
                tipoPropiedadModel.Error = -1;
                tipoPropiedadModel.Mensaje = ex.Message;
            }
            return tipoPropiedadModel;
        }

        public bool Actualizar(TipoPropiedadModel tipoPropiedadModel)
        {
            try
            {
                var tipoPropiedad = mapper.Map<TipoPropiedad>(tipoPropiedadModel);
                using(DB_A645C6_ImmobilisEntities entities = new DB_A645C6_ImmobilisEntities())
                {
                    entities.Entry(tipoPropiedad).State = System.Data.Entity.EntityState.Modified;
                    var resp = entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex) 
            {
                tipoPropiedadModel.Error = -1;
                tipoPropiedadModel.Mensaje = ex.Message;
            }
            return false;
        }

        public TipoPropiedadModel Leer(int tipoPropiedadId)
        {
            TipoPropiedadModel tipoPropiedadModel = new TipoPropiedadModel();
            try
            {
                var tipoPropiedad = entities.TipoPropiedad.Where(c => c.TipoPropiedadId == tipoPropiedadId).FirstOrDefault();
                if (tipoPropiedad != null)
                {
                    tipoPropiedadModel = mapper.Map<TipoPropiedadModel>(tipoPropiedad);
                }
            }
            catch (Exception ex)
            {
                tipoPropiedadModel.Error = -1;
                tipoPropiedadModel.Mensaje = ex.Message;
            }
            return tipoPropiedadModel;
        }

        public List<TipoPropiedadModel> Listar()
        {
            List<TipoPropiedadModel> lista = new List<TipoPropiedadModel>();
            try
            {
                var tipoPropiedadList = entities.TipoPropiedad.ToList();
                if (tipoPropiedadList != null && tipoPropiedadList.Count() > 0)
                {
                    lista = mapper.Map<List<TipoPropiedadModel>>(tipoPropiedadList);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return lista;
        }


    }
}
