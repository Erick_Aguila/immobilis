﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiBanco.Models.ViewModel
{
    public class PagoViewModel
    {
        public int BancoId { get; set; }
        [Required(ErrorMessage = "El campo Nombre Banco id es obligatorio.")]
        public string NombreBanco { get; set; }
        [Required(ErrorMessage = "El campo Rut es obligatorio.")]
        public string Rut { get; set; }
        [Required(ErrorMessage = "El campo Monto pie es obligatorio.")]
        public string MontoPie { get; set; }
        [Required(ErrorMessage = "El campo Clave es obligatorio.")]
        public string Clave { get; set; }
    }
}