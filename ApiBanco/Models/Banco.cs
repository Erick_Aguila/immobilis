//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ApiBanco.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Banco
    {
        public int BancoId { get; set; }
        public string NombreBanco { get; set; }
        public string RutCliente { get; set; }
        public string NumeroCuenta { get; set; }
        public Nullable<int> Saldo { get; set; }
        public Nullable<int> Clave { get; set; }
    }
}
