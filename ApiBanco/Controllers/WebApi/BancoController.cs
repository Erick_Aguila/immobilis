﻿using ApiBanco.Models;
using ApiBanco.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApiBanco.Controllers.WebApi
{
    public class BancoController : ApiController
    {
        // GET: api/Banco/5
        [HttpGet]
        public IHttpActionResult Get(string rutCliente)
        {
            try
            {
                BancoEntities entities = new BancoEntities();
                var lista = entities.Banco.Where(c => c.RutCliente == rutCliente).ToList();
                return Json(lista);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        // POST: api/Banco
        [HttpPost]
        public HttpResponseMessage Post([FromBody]PagoViewModel pagoViewModel)
        {
            var resp = 0;
            HttpResponseMessage msg = null;
            if (ModelState.IsValid)
            {
                try
                {
                    using (BancoEntities bancoEntities = new BancoEntities())
                    {
                        int clave = Convert.ToInt32(pagoViewModel.Clave);
                        var cliente = bancoEntities.Banco.Where(c => c.RutCliente == pagoViewModel.Rut && c.Clave == clave && c.NombreBanco == pagoViewModel.NombreBanco).FirstOrDefault();
                        if (cliente != null)
                        {
                            //descontamos el dinero
                            if (cliente.Saldo > Convert.ToInt32(pagoViewModel.MontoPie))
                            {
                                cliente.Saldo = (cliente.Saldo - Convert.ToInt32(pagoViewModel.MontoPie));
                                bancoEntities.Entry(cliente).State = System.Data.Entity.EntityState.Modified;
                                resp = bancoEntities.SaveChanges();
                                msg = Request.CreateResponse(HttpStatusCode.OK, resp);
                            }
                            else
                            {
                                //Saldo insuficiente
                                msg = Request.CreateErrorResponse(HttpStatusCode.Conflict, "Cliente no se encuentra registrado");
                            }
                        }
                        else
                        {
                            msg = Request.CreateErrorResponse(HttpStatusCode.NotFound, "Los datos del cliente son incorrectos");
                        }
                    }
                }
                catch (Exception ex)
                {
                    msg = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
            else
            {
                msg = Request.CreateErrorResponse(HttpStatusCode.NotAcceptable, "Modelo no válido");
            }
            return msg;
        }

    }
}
