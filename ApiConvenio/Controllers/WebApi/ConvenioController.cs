﻿using ApiConvenio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApiConvenio.Controllers.WebApi
{
    public class ConvenioController : ApiController
    {
        // GET: api/Convenio
        public IHttpActionResult Get()
        {
            try
            {
                ConvenioEntities entities = new ConvenioEntities();
                var lista = entities.Convenio.ToList();
                return Json(lista);
            }
            catch (Exception ex)
            {
                throw;
            }
            //return Json(string.Empty);//
        }

        // GET: api/Convenio/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Convenio
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Convenio/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Convenio/5
        public void Delete(int id)
        {
        }
    }
}
