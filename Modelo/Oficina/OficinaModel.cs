﻿using Modelo.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Oficina
{
    public class OficinaModel: BaseModel
    {
        public int OficinaId { get; set; }
        [Required(ErrorMessage = "El campo Metros cuadrados es obligatorio.")]
        public float MetrosCuadrados { get; set; }
        [Required(ErrorMessage = "El campo Tiene patente comercial es obligatorio.")]
        public bool TienePatenteComercial { get; set; }
        [Required(ErrorMessage = "El campo Activo es obligatorio.")]
        public bool Activo { get; set; }

        public OficinaModel()
        {
            OficinaId = 0;
            MetrosCuadrados = 0;
            TienePatenteComercial = false;
            Activo = false;
        }

    }
}
