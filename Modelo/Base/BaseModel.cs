﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Base
{
    public class BaseModel
    {
        public int Error { get; set; }
        public string Mensaje { get; set; }
        public string Detalle { get; set; }

        public BaseModel()
        {
            Error = 0;
            Mensaje = string.Empty;
            Detalle = string.Empty;
        }
        
    }
}
