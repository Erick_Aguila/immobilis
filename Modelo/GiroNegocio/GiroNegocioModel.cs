﻿using Modelo.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.GiroNegocio
{
    public class GiroNegocioModel : BaseModel
    {
        public int GiroNegocioId { get; set; }
        [Required(ErrorMessage = "El campo Descripción es obligatorio.")]
        [MaxLength(ErrorMessage = "El campo Descripción tiene un máximo de 50 carácteres.")]
        public string Descripcion { get; set; }
        [Required(ErrorMessage = "El campo Activo es obligatorio.")]
        public bool Activo { get; set; }

        public GiroNegocioModel()
        {
            GiroNegocioId = 0;
            Descripcion = string.Empty;
            Activo = false;
        }

    }
}
