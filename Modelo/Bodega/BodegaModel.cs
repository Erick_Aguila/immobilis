﻿using Modelo.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Bodega
{
    public class BodegaModel: BaseModel
    {
        public int BodegaId { get; set; }
        [Required(ErrorMessage = "El campo Metros Cuadrados es obligatorio")]
        public float MetrosCuadrados { get; set; }
        [Required(ErrorMessage = "El campo Actio es obligatorio.")]
        public bool Activo { get; set; }

        public BodegaModel()
        {
            BodegaId = 0;
            MetrosCuadrados = 0;
            Activo = false;
        }

    }
}
