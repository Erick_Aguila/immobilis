﻿using Modelo.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Propiedad
{
    public class PropiedadDisponibleModel : BaseModel
    {
        public List<PropiedadModel> ListaDepartamentos { get; set; }
        public List<PropiedadModel> ListaCasas { get; set; }
        public List<PropiedadModel> ListaBodegas { get; set; }
        public List<PropiedadModel> ListaOficinas { get; set; }
        public List<PropiedadModel> ListaEstacionamientos { get; set; }
    }
}
