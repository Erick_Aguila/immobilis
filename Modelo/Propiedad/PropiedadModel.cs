﻿using Modelo.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Propiedad
{
    public class PropiedadModel : BaseModel
    {
        [Required(ErrorMessage = "El campo PropiedadId es obligatorio")]
        public Guid PropiedadId { get; set; }
        [Required(ErrorMessage = "El campo Dirección es obligatorio.")]
        [MaxLength(200,ErrorMessage = "El campo Ubicación tiene como máximo 200 carácteres")]
        public string Direccion { get; set; }
        [Required(ErrorMessage = "El campo Estado Propiedad Id es obligatorio.")]
        public int EstadoPropiedadId { get; set; }
        [Required(ErrorMessage = "El campo Tipo Propiedad Id es obligatorio.")]
        public int TipoPropiedadId { get; set; }
        public Nullable<int> Tasacion { get; set; }
        [MaxLength(100,ErrorMessage = "El campo Notaria tiene como máximo 100 carácteres")]
        public string Notaria { get; set; }
        [MaxLength(500, ErrorMessage = "El campo Conservador de Bienes tiene como máximo 500 carácteres")]
        public string ConservadorBienes { get; set; }
        [Required(ErrorMessage = "El campo Giro Negocio Id es obligatorio.")]
        public Nullable<int> GiroNegocioId { get; set; }
        [Required(ErrorMessage = "El campo Región es ogligatorio.")]
        public int RegionId { get; set; }
        [Required(ErrorMessage = "El campo Comuna es ogligatorio.")]
        public int ComunaId { get; set; }
        public Nullable<int> PrecioUf { get; set; }
        public Nullable<bool> EsPropiedadDeTercero { get; set; }
        public int BancoId { get; set; }
        public Nullable<bool> UtilizaCreditoHipotecario { get; set; }
        public Nullable<Guid> DepartamentoId { get; set; }
        public Nullable<Guid> CasaId { get; set; }
        public Nullable<Guid> BodegaId { get; set; }
        public Nullable<Guid> EstacionamientoId { get; set; }
        public Nullable<Guid> OficinaId { get; set; }
        public Nullable<Guid> UsuarioId { get; set; }


        public EstadoPropiedad.EstadoPropiedadModel EstadoPropiedad { get; set; }
        public TipoPropiedad.TipoPropiedadModel TipoPropiedad { get; set; }
        public Region.RegionModel Region { get; set; }
        public Comuna.ComunaModel Comuna { get; set; }
        public Banco.BancoModel Banco { get; set; }
        public Departamento.DepartamentoModel Departamento { get; set; }
        public Casa.CasaModel Casa { get; set; }
        public Bodega.BodegaModel Bodega { get; set; }
        public Estacionamiento.EstacionamientoModel Estacionamiento { get; set; }
        public Oficina.OficinaModel Oficina { get; set; }

        public PropiedadModel()
        {
            PropiedadId = Guid.Empty;
            Direccion = string.Empty;
            EstadoPropiedadId = 0;
            TipoPropiedadId = 0;
            Tasacion = 0;
            Notaria = string.Empty;
            ConservadorBienes = string.Empty;
            GiroNegocioId = 0;
            RegionId = 0;
            ComunaId = 0;
            PrecioUf = 0;
            EsPropiedadDeTercero = false;
            BancoId = 0;
            UtilizaCreditoHipotecario = false;
            DepartamentoId = Guid.Empty;
            CasaId = Guid.Empty;
            BodegaId = Guid.Empty;
            EstacionamientoId = Guid.Empty;
            OficinaId = Guid.Empty;
            UsuarioId = Guid.Empty;

        }

    }
}
