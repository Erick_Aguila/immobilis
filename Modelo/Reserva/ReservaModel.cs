﻿using Modelo.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Reserva
{
    public class ReservaModel :BaseModel
    {
        public Guid ReservaId { get; set; }
        [Required(ErrorMessage = "El campo Propiedad Id es obligatorio.")]
        public Guid PropiedadId { get; set; }
        [Required(ErrorMessage = "El campo Usuario Id es obligatorio.")]
        public Guid UsuarioId { get; set; }
        [Required(ErrorMessage = "El campo Pago Id es obligatorio.")]
        public Guid PagoId { get; set; }
        [Required(ErrorMessage = "El campo Porcentaje Reserva es obligatorio.")]
        public int PorcentajeReserva { get; set; }

        public Propiedad.PropiedadModel Propiedad { get; set; }
        public Usuario.UsuarioModel Usuario { get; set; }
        public Pago.PagoModel Pago { get; set; }

        public ReservaModel()
        {
            ReservaId = Guid.Empty;
            PropiedadId = Guid.Empty;
            UsuarioId = Guid.Empty;
            PorcentajeReserva = 0;
        }

    }
}
