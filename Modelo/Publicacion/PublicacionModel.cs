﻿using Modelo.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Publicacion
{
    public class PublicacionModel : BaseModel
    {
        public Guid PublicacionId { get; set; }
        [Required(ErrorMessage = "El campo PropiedadId es obligatorio")]
        public Guid PropiedadId { get; set; }
        [Required(ErrorMessage = "El campo Fecha publicación es obligatorio")]
        public DateTime FechaPublicacion { get; set; }
        [Required(ErrorMessage = "El campo Archivo Id es obligatorio")]
        public int ArchivoId { get; set; }

        public Propiedad.PropiedadModel Propiedad { get; set; }
        public Archivo.ArchivoModel Archivo { get; set; }

        public PublicacionModel()
        {
            PublicacionId = Guid.Empty;
            PropiedadId = Guid.Empty;
            FechaPublicacion = DateTime.Now;
            ArchivoId = 0;
        }

    }
}
