﻿using Modelo.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Usuario
{
    public class UsuarioModel: BaseModel
    {
        public Guid UsuarioId { get; set; }
        [Required(ErrorMessage = "El campo Rut es obligatorio.")]
        [MaxLength(20,ErrorMessage = "El campo Rut tiene un máximo de 20 carácteres")]
        public string Rut { get; set; }
        [Required(ErrorMessage = "El campo Nombre es obligatorio.")]
        [MaxLength(100, ErrorMessage = "El campo Nombres tiene un máximo de 100 carácteres")]
        public string Nombres { get; set; }
        [Required(ErrorMessage = "El campo Apellido Paterno es obligatorio.")]
        [MaxLength(50, ErrorMessage = "El campo Apellido Paterno tiene un máximo de 50 carácteres")]
        public string ApellidoPaterno { get; set; }
        [Required(ErrorMessage = "El campo Apellido Materno es obligatorio.")]
        [MaxLength(50, ErrorMessage = "El campo Apellido Materno tiene un máximo de 50 carácteres")]
        public string ApellidoMaterno { get; set; }
        [Required(ErrorMessage = "El campo Email es obligatorio.")]
        [MaxLength(100, ErrorMessage = "El campo Email tiene un máximo de 100 carácteres")]
        public string Email { get; set; }
        [Required(ErrorMessage = "El campo Clave es obligatorio.")]
        [MaxLength(200, ErrorMessage = "El campo Clave tiene un máximo de 200 carácteres")]
        public string Clave { get; set; }
        public Nullable<int> Celular { get; set; }
        public int TipoUsuarioId { get; set; }

        public TipoUsuario.TipoUsuarioModel TipoUsuario { get; set; }

        public UsuarioModel()
        {
            UsuarioId = Guid.Empty;
            Rut = string.Empty;
            Nombres = string.Empty;
            ApellidoPaterno = string.Empty;
            ApellidoMaterno = string.Empty;
            Email = string.Empty;
            Clave = string.Empty;
            Celular = 0;
            TipoUsuarioId = 0;
        }

    }
}
