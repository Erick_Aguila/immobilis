﻿using Modelo.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Departamento
{
    public class DepartamentoModel : BaseModel
    {
        public Guid DepartamentoId { get; set; }
        [Required(ErrorMessage = "El campo Nombre Edificio es obligatorio.")]
        [MaxLength(50, ErrorMessage = "El campo Nombre Edificio debe tener un máximo de 50 carácteres.")]
        public string NombreEdificio { get; set; }
        [Required(ErrorMessage = "El campo Cantidad dormitorios es obligatorio.")]
        public int CantidadDormitorios { get; set; }
        [Required(ErrorMessage = "El campo Cantidad baños es obligatorio.")]
        public int CantidadBanios { get; set; }
        [Required(ErrorMessage = "El campo Metros cuadrados es obligatorio.")]
        public float MetrosCuadrados { get; set; }
        [Required(ErrorMessage = "El campo Activo es obligatorio.")]
        public bool Activo { get; set; }
        [Required(ErrorMessage = "El campo Fecha creación es obligatorio.")]
        public DateTime FechaCreacion { get; set; }

        public DepartamentoModel()
        {
            DepartamentoId = Guid.Empty;
            NombreEdificio = string.Empty;
            CantidadDormitorios = 0;
            CantidadBanios = 0;
            MetrosCuadrados = 0;
            Activo = false;
            FechaCreacion = DateTime.Now;
        }

    }
}
