﻿using Modelo.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Archivo
{
    public class ArchivoModel : BaseModel
    {
        public int ArchivoId { get; set; }
        [Required(ErrorMessage ="El campo Nombre del Archivo es requerido")]
        [MaxLength(200,ErrorMessage = "El campo Nombre de Archivo debe tener un máximo de 200 carácteres.")]
        public string NombreArchivo { get; set; }
        [Required(ErrorMessage = "El campo Descripción es requerido")]
        [MaxLength(200, ErrorMessage = "El campo Descripción debe tener un máximo de 200 carácteres.")]
        public string Descripcion { get; set; }
        [Required(ErrorMessage = "El campo Ruta es requerido")]
        [MaxLength(200, ErrorMessage = "El campo Ruta debe tener un máximo de 200 carácteres.")]
        public string Ruta { get; set; }

        public ArchivoModel()
        {
            ArchivoId = 0;
            NombreArchivo = string.Empty;
            Descripcion = string.Empty;
            Ruta = string.Empty;
        }


    }
}
