﻿using Modelo.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Agenda
{
    public class AgendaModel : BaseModel
    {
        public int AgendaId { get; set; }
        [Required(ErrorMessage ="El campo Reserva Id es obligatorio.")]
        public Guid ReservaId { get; set; }
        [Required(ErrorMessage = "El campo Fecha Agenda es obligatorio.")]
        public DateTime FechaAgenda { get; set; }
        [Required(ErrorMessage = "El campo Fecha Creación es obligatorio.")]
        public DateTime FechaCreacion { get; set; }

        public Reserva.ReservaModel Reserva { get; set; }

        public AgendaModel()
        {
            AgendaId = 0;
            ReservaId = Guid.Empty;
            FechaAgenda = new DateTime();
            FechaCreacion = DateTime.Now;
        }

    }
}
