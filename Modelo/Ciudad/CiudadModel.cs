﻿using Modelo.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Ciudad
{
    public class CiudadModel: BaseModel
    {
        public int CiudadId { get; set; }
        public int RegionId { get; set; }
        [Required(ErrorMessage = "El campo Nombre ciudad es obligatorio.")]
        [MaxLength(50, ErrorMessage = "El campo Nombre ciudad debe tener un máximo de 50 carácteres.")]
        public string NombreCiudad { get; set; }
        public bool Activo { get; set; }

        public Region.RegionModel Region { get; set; }

        public CiudadModel()
        {
            CiudadId = 0;
            RegionId = 0;
            NombreCiudad = string.Empty;
            Activo = false;
        }

    }
}
