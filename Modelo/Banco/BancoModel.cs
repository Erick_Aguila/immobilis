﻿using Modelo.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Banco
{
    public class BancoModel: BaseModel
    {
        public int BancoId { get; set; }
        [Required(ErrorMessage ="El campo Nombre Banco es requerido.")]
        [MaxLength(50, ErrorMessage = "El campo Nombre Banco debe tener un máximo de 50 carácteres.")]
        public string NombreBanco { get; set; }
        [Required(ErrorMessage ="El campo Activo es requerido.")]
        public bool Activo { get; set; }

        public BancoModel()
        {
            BancoId = 0;
            NombreBanco = string.Empty;
            Activo = false;
        }

    }
}
