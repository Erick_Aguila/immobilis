﻿using Modelo.Base;
using Modelo.Propiedad;
using Modelo.Reserva;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Contrato
{
    public class ContratoModel: BaseModel
    {
        public Guid ContratoId { get; set; }
        public Guid PropiedadId { get; set; }
        public Guid ReservaId { get; set; }
        public DateTime FechasCreacion { get; set; }

        public PropiedadModel Propiedad { get; set; }
        public ReservaModel Reserva { get; set; }

        public ContratoModel()
        {
            ContratoId = Guid.Empty;
            PropiedadId = Guid.Empty;
            ReservaId = Guid.Empty;
            FechasCreacion = DateTime.Now;
        }

    }
}
