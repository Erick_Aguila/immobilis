﻿using Modelo.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Pago
{
    public class PagoModel : BaseModel
    {
        public Guid PagoId  { get; set; }
        [Required(ErrorMessage ="El campo Monto es obligatorio")]
        public int Monto { get; set; }
        [Required(ErrorMessage = "El campo Tarjeta es obligatorio")]
        public string Tarjeta { get; set; }
        public int TipoCuentaId { get; set; }

        public TipoCuenta.TipoCuentaModel TipoCuenta { get; set; }

        public PagoModel()
        {
            PagoId = Guid.Empty;
            Monto = 0;
            Tarjeta = string.Empty;
            TipoCuentaId = 0;
        }

    }
}
