﻿using Modelo.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Region
{
    public class RegionModel : BaseModel
    {
        public int RegionId { get; set; }
        [Required(ErrorMessage = "El campo Nombre región es obligatorio")]
        [MaxLength(200,ErrorMessage = "El campo Nombre región debe tener un máximo de 200 carácteres.")]
        public string NombreRegion { get; set; }
        [Required(ErrorMessage = "El campo Activo es obligatorio")]
        public bool Activo { get; set; }

        public RegionModel()
        {
            RegionId = 0;
            NombreRegion = string.Empty;
            Activo = false;
        }

    }
}
