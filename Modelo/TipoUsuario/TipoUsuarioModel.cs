﻿using Modelo.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.TipoUsuario
{
    public class TipoUsuarioModel : BaseModel
    {
        public int TipoUsuarioId { get; set; }
        public string Nombre { get; set; }
        public bool Activo { get; set; }

        public TipoUsuarioModel()
        {
            TipoUsuarioId = 0;
            Nombre = string.Empty;
            Activo = false;
        }

    }
}
