﻿using Modelo.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Casa
{
    public class CasaModel : BaseModel
    {
        public int CasaId { get; set; }
        [Required(ErrorMessage = "El campo Nombre Conjunto es obligatorio.")]
        [MaxLength(50,ErrorMessage = "El campo Nombre Conjunto debe tener un máximo de 50 carácteres.")]
        public string NombreConjunto { get; set; }
        [Required(ErrorMessage = "El campo Cantidad Dormitorio es obligatorio.")]
        public int CantidadDormitorio { get; set; }
        [Required(ErrorMessage = "El campo Cantidad Baños es obligatorio.")]
        public int CantidadBanios { get; set; }
        [Required(ErrorMessage = "El campo Metros cuadrados habilitados es obligatorio.")]
        public float MetrosCuadradosHabilitable { get; set; }
        [Required(ErrorMessage = "El campo Metros cuadrados patio es obligatorio.")]
        public float MetrosCuadradosPatio { get; set; }
        [Required(ErrorMessage = "El campo Activo es obligatorio.")]
        public bool Activo { get; set; }
        [Required(ErrorMessage = "El campo Fecha creación es obligatorio.")]
        public DateTime FechaCreacion { get; set; }

        public CasaModel()
        {
            CasaId = 0;
            NombreConjunto = string.Empty;
            CantidadDormitorio = 0;
            CantidadBanios = 0;
            MetrosCuadradosHabilitable = 0;
            MetrosCuadradosPatio = 0;
            Activo = false;
            FechaCreacion = DateTime.Now;
        }

    }
}
