﻿using Modelo.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.TipoCuenta
{
    public class TipoCuentaModel : BaseModel
    {
        public int TipoCuentaId { get; set; }
        [Required(ErrorMessage = "El campo Descripción es obligatorio.")]
        [MaxLength(50, ErrorMessage = "El campo Descripción debe tener un máximo de 50 carácteres.")]
        public string Descripcion { get; set; }
        [Required(ErrorMessage = "El campo Activo es obligatorio.")]
        public bool Activo { get; set; }

        public TipoCuentaModel()
        {
            TipoCuentaId = 0;
            Descripcion = string.Empty;
            Activo = false;
        }

    }
}
