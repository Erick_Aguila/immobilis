﻿using Modelo.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Estacionamiento
{
    public class EstacionamientoModel: BaseModel
    {
        public int EstacionamientoId { get; set; }
        [Required(ErrorMessage = "El campo Nivel ubicación es obligatorio.")]
        public int NivelUbicacion { get; set; }
        [Required(ErrorMessage = "El campo Activo es obligatorio.")]
        public bool Activo { get; set; }

        public EstacionamientoModel()
        {
            EstacionamientoId = 0;
            NivelUbicacion = 0;
            Activo = false;
        }

    }
}
