﻿using Modelo.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Comuna
{
    public class ComunaModel : BaseModel
    {
        public int ComunaId { get; set; }
        public int RegionId { get; set; }
        [Required(ErrorMessage = "El campo Nombre comuna es obligatorio.")]
        [MaxLength(50, ErrorMessage = "El campo Nombre comuna debe tener un máximo de 50 carácteres.")]
        public string NombreComuna { get; set; }
        public bool Activo { get; set; }

        public Region.RegionModel Region { get; set; }

        public ComunaModel()
        {
            ComunaId = 0;
            RegionId = 0;
            NombreComuna = string.Empty;
            Activo = false;
        }

    }
}
